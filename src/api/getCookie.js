/**
 * Created by lg on 2017-05-10.
 */
const getCookie = (name) => {
    if (typeof document.cookie === "undefined" || document.cookie ==='') return {};

    // decode base64 & parse json

    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) {
        let sessionData = parts.pop().split(";").shift();
        sessionData    = JSON.parse(atob(sessionData));
        return sessionData;
    }
};

export default getCookie;