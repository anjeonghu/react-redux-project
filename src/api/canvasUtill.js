/**
 * Created by front on 2017-06-09.
 */

import vivarSDK from './vivarSDK';
import html2canvas from 'html2canvas'

const canvasConfig = ()=>{

    let cx = 0;
    let cy = 0;
    let sx = 0;
    let sy = 0;
    let sw = 0;
    let sh = 0;
    let width = 0;

    let sRatio=0;
    let rsx =0;
    let rsy =0;

    const getRealCoordinate = (obj) =>{
        let x=0,y=0;
        if(rsx > sx) x = obj.x + rsx;
        if(rsx === sx) x=  obj.x;
        if(rsy > sy) y =  obj.y + rsy;
        if(rsy === sy) y =  obj.y;

        return {x, y}
    };

    const imageDrawingCover = (ctx, img, x, y, width, height, pos, opts) =>{
        opts = Object.assign({ cx: cx, cy: cy, zoom: 1, alpha: 1 }, opts || {});

        cx = parseInt(pos[0])/100;
        cy = parseInt(pos[1])/100;

        if (cx < 0 || cx > 1) throw new Error('Make sure 0 < opt.cx < 1 ');
        if (cy < 0 || cy > 1) throw new Error('Make sure 0 < opt.cy < 1 ');
        if (opts.zoom < 1) throw new Error('opts.zoom not >= 1');

        const ir = img.width / img.height;
        const r = width / height;

        sw = (ir < r ? img.width : img.height * r) / opts.zoom;
        sh = (ir < r ? img.width / r : img.height) / opts.zoom;
        sx = (img.width - sw) * cx;
        sy = (img.height - sh) * cy;
        sRatio  = width/sw;
        rsx = sx * sRatio;
        rsy = sy * sRatio;

        ctx.save();
        ctx.globalAlpha = opts.alpha;
        ctx.drawImage(img, sx, sy, sw, sh, x, y, width, height);
        ctx.restore();
    };

    const imageCapture = (canvas, video) => {
        let context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.drawImage(video, 0, 0, canvas.width,
            canvas.height);
    };

    return {
        getRealCoordinate:getRealCoordinate,
        imageDrawingCover:imageDrawingCover,
        imageCapture:imageCapture
    }

};
const canvasUtil = canvasConfig();
export default canvasUtil;