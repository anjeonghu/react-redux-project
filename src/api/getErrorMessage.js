/**
 * Created by lg on 2017-05-10.
 */
const getErrorMessage = (result) => {
    const errorcode = {
        401:'Unauthorized',
        404:'member Key not equal',
        409:'It is already used, Please use another ID',
        500:'Server Error',
        1001: 'INVALID_SIGNATURE',
        1003: 'INVALID_STATE',
        1005: 'INVALID_LOGIN_ID',
        1006: 'INVALID_GROUP_ID',
        4001: 'ERROR_RTC_INVALID_PEERCONNECTION',
        4002: 'ERROR_RTC_SEND_DATA ',
        9999: 'unknown error'
    };
    return result + ': ' + errorcode[result];
};

export default getErrorMessage;