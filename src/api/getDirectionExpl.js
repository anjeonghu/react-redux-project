/**
 * Created by lg on 2017-05-10.
 */
const getDirectionExpl = (direction) => {
    const directions = {
        'PeopleMoveForward' :'Move forward',
        'PeopleMoveBackward': 'Move Backward',
        'CameraLeft' : 'Move the camera left',
        'CameraUp': 'Move up the camera',
        'CameraRight': 'Move the camera to the right' ,
        'CameraDown': 'Move down the camera',
        'CameraUpperLeft': 'Move the camera to the Upper Left',
        'CameraUpperRight': 'Move the camera to the Upper Right',
        'CameraLowerLeft': 'Move the camera to the Lower Left',
        'CameraLowerRight': 'Move the camera to the Lower Right',
        'ObjectLeftSide': 'View left side of the object',
        'ObjectTopView': 'View top of the object',
        'ObjectRightSide':'View right side of the object',
        'ObjectBottomView':'View bottom of the object',
        'FeelGood': 'GOOD!'
    };

    return directions[direction];
};

export default getDirectionExpl;