/**
 * Created by lg on 2017-05-10.
 */


import {sign} from '../common';


class VivarSDK {

    static get instance() {

        let vivar = Symbol(); //symbol 키로 한 속성은 obj.name으로 접근 할 수 없다.
        const Vivar = window.Vivar;
        let _config = {};
        _config.sign = sign;

        if(!this[vivar]){

            this[vivar] = new Vivar(_config);

            return this[vivar];
        }
    }
}
/*const Vivar = window.Vivar;
window.vivarSDK = new Vivar({sign:sign});*/
export default VivarSDK.instance;