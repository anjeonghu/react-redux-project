/**
 * Created by front on 2017-05-02.
 */
import React from 'react';
import Header from '../containers/Header';
import './layout.css';

const CommonLayout = (props) => {
    return (
       <div className="row">
            <div className="wrapper">
                <main className="content">
                    {props.children}
                </main>
            </div>
       </div>
    );
};


const CommonLayoutWithHeader = (props) => {
    return (
    <div>
        <div className="row vivar-main">
            <header>
                <Header/>
            </header>
            {props.children}
        </div>

    </div>
    );
};

export { CommonLayout, CommonLayoutWithHeader};

