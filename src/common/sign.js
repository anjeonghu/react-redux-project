/**
 * Created by front on 2017-05-02.
 */
// 로그인 키

let loc = window.location;

let sign;
if (loc.hostname === 'dev.vivar.me') {
    sign = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdCI6IldlYiIsImciOiJkOGI2OTliNWQ5MzM0ZjhmYTg3NzQ3NjM5Y2U0MmFhZCJ9.E7cqEsmGyvmsJEgPo400ol8vVZ6r6CSMEqSt71PVV80.poeFEyh89RXjyf0G8nKiUDNweokUY8aiVNX3zVccJSM=';
} else if (loc.hostname === 'test.vivar.me') {
    sign = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdCI6IldlYiIsImciOiJjNDZhNGFhMjRlOTg0OGU0OTkyNWZiMjM5YmQ0ZGFlMCJ9.SlQ5PJybZm94cy76CHqSZ1DzhFWw9WPWwm4J5myqojQ.87NV15vKYrPwpZMBrTsH5YYkM4FLa99kLDvO/FccYCg=';
} else if (loc.hostname === '127.0.0.1') {
    sign = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdCI6IldlYiIsImciOiJmNWVjN2JmZjIzYTY0ZDM5YmI2NjkwNzI4ZGY2NWMwZCJ9.o1uVZnV2dGvEAdTyDpka0IPh-Wju498hDnty2E_67CU.nRe6/y8CvMiv1saJn6gHhWvX/kZ8I14Vz+4V9mK6GE0=';
}

export default sign;