/**
 * Created by lg on 2017-05-05.
 */
import React from 'react';
import {connect} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';

import {getCookie} from '../api';


/*const AuthRoute = ({component: Component, ...props}) => {


    let redirect = false;
    /!*    let sessionData = getCookie('key');
     if(sessionData !== undefined){
     redirect = (sessionData.isJoinedGroup && sessionData.isLoggedIn);
     }*!/
    if (!props.loginStatus) {
        redirect = true;
    }
    return (
        <Route {...props} render={
            props => (
                redirect ? ( <Redirect to={{
                    pathname: '/',
                    state: {from: props.location}
                }}/>) :
                    (
                        <Component {...props}/>
                    )
            )}/>
    )
};*/

const AuthRoute = ({component: Component, ...props}) => {


    let redirect = false;

    if (!props.groupJoinStatus) {
        redirect = true;
    }
    /*    let sessionData = getCookie('key');
     if(sessionData.isJoinedGroup !== undefined){
     redirect = sessionData.isJoinedGroup;
     }*/
    return (
        <Route {...props} render={
            props => (
                redirect ? (
                    <Redirect to={{
                        pathname: '/',
                        state: {from: props.location}
                    }}/>
                ) : (

                    <Component {...props}/>
                )
            )}/>
    )
};

export {AuthRoute};
