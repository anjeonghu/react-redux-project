/**
 * Created by front on 2017-04-28.
 */
import React from 'react';
import { connect } from 'react-redux';
//components
import Authentication from '../components/Authentication';
import { groupJoinRequest, groupJoinSuccess } from '../actions/authentication';
import {CommonLayout} from '../common';


class GroupJoin extends React.Component {
    constructor(props) {
        super(props);
        this.handleGroupJoin = this.handleGroupJoin.bind(this);
    }

    handleGroupJoin(groupName, password) {
        return this.props.groupJoinRequest(groupName, password).then(
            () => {
                if(this.props.groupStatus === "SUCCESS") {

                    this.props.history.replace('/group/'+groupName);
                    return true;
                } else {
                    return false;
                }
            }

        );
    }

    render() {
        return (
            <CommonLayout>
                    <Authentication
                        mode={true}
                        onGroupJoin={this.handleGroupJoin}
                    />
            </CommonLayout>
        );
    }
}
GroupJoin.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const mapStateToProps  = (state) => {
  return {
      groupStatus: state.authentication.group.status
  }
};


const mapDispatchToProps = (dispatch) => {
    return {
        groupJoinRequest: (id, pw) => {
            return dispatch(groupJoinRequest(id, pw));
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(GroupJoin) ;