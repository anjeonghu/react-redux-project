/**
 * Created by front on 2017-04-28.
 */
import React from 'react';
import {connect} from 'react-redux';
import {CommonLayoutWithHeader}  from '../common';
import $ from 'jquery';
//components
import {GroupMembers, SendAndAcceptCall}  from '../components';
import {groupMembersInit, getGroupMembersRequest, getGroupMembersUpdate} from '../actions/getGroupMembers';
import {loginRequest} from '../actions/authentication';
import {sendCallRequest, acceptCallRequest, callInit,  rejectCall, cancelCall, endCall} from '../actions/sendAndAcceptCall';

import { vivarSDK } from '../api';

class Home extends React.Component {

    constructor(props) {
        super(props);

        this.handleGroupMembersInit = this.handleGroupMembersInit.bind(this);
        this.handleCallInit = this.handleCallInit.bind(this);

        this.handleGetGroupMembers = this.handleGetGroupMembers.bind(this);
        this.handleSendCall = this.handleSendCall.bind(this);

        this.handleAcceptCall = this.handleAcceptCall.bind(this);

        this.handleRejectCall = this.handleRejectCall.bind(this);
        this.handleCancelCall = this.handleCancelCall.bind(this);
        this.handleEndCall = this.handleEndCall.bind(this);
    }

    componentWillMount() {
        /* 테스트용  */

        if(this.props.loginStatus === false && window.localStorage.getItem('sessionData') !== null){
                let sessionData  = window.localStorage.getItem('sessionData');
                sessionData = JSON.parse(sessionData);
                let userName = sessionData.userName;
                let groupName = sessionData.groupName;
                let password = sessionData.password;
            this.props.loginRequest(userName, groupName, password).then(()=>{
                if(this.props.loginStatus){
                    let groupId = this.props.groupId;
                    this.handleGetGroupMembers(groupId);
                }
            });
        }
        else{
            let groupId = this.props.groupId;
            this.handleGetGroupMembers(groupId);
        }
        /* 테스트용 end */

    }
    componentDidMount() {

        //브라우저 뒤로 가기 버튼 클릭 시 실행
        // window.onbeforeunload  = function(e){
        //     return "are you sure to exit??";
        // };

        $(".home_loading_block").css({'display':'none'});

        this.handleGroupMembersInit();
        this.handleCallInit();
    }

    handleGroupMembersInit() {
        this.props.groupMembersInit().then(
            () => {
            }
        );
    }
    handleCallInit() {
        this.props.callInit().then(
            () => {
            }
        );
    }


    handleGetGroupMembers(groupId) {
        this.props.getGroupMembersRequest(groupId).then(
            () => {
            }
        );
    }
    handleSendCall(remote) {
        this.props.sendCallRequest(remote).then(
            () => {
            }
        );
    }
    handleAcceptCall() {
        // 전화 받기
        let remoteName = this.props.memberList[this.props.remoteId].memberId;
        remoteName = remoteName.split('_')[1];

        this.props.acceptCallRequest(remoteName).then(
            () => {
                if(this.props.callStatus === "CALL_CONNECTED"){

                    console.log('onCallConnected');

                    document.getElementById('root-container').style.display = 'none';
                    document.getElementById('root-container-vivar').style.display = 'block';

                }
            }
        );
    }
    handleRejectCall() {
        //전화 거절
        this.props.rejectCall();
    }
    handleCancelCall() {
        //전화 연결 취소
        this.props.cancelCall();
    }
    handleEndCall() {
        //전화 끊기
        this.props.endCall();
    }

    render() {
        return (
            <CommonLayoutWithHeader>
                <GroupMembers
                    mode={this.props.callStatus}
                    userName={this.props.userName}
                    data={this.props.memberList}
                    onSendCall={this.handleSendCall}
                />
                <SendAndAcceptCall
                    mode={this.props.callStatus}
                    remoteId={this.props.remoteId}
                    memberList={this.props.memberList}
                    onAcceptCall={this.handleAcceptCall}
                    onRejectCall={this.handleRejectCall}
                    onEndCall={this.handleEndCall}
                    onCancelCall={this.handleCancelCall}
                />
            </CommonLayoutWithHeader>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        loginStatus: state.authentication.status.isLoggedIn,
        groupId: state.authentication.status.groupId,
        groupName: state.authentication.status.groupName,
        userName: state.authentication.status.userName,
        memberList: state.getGroupMembers.groupInfo.memberList,
        getMembersStatus: state.getGroupMembers.status,
        remoteId: state.sendAndAcceptCall.callInfo.remoteId,
        callStatus: state.sendAndAcceptCall.status
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        loginRequest:(userName, groupName, password) => {
            return dispatch(loginRequest(userName, groupName, password));
        },
        getGroupMembersRequest: (groupId) => {
            return dispatch(getGroupMembersRequest(groupId));
        },
        callInit: () => {
            return dispatch(callInit());
        },
        groupMembersInit: () => {
            return dispatch(groupMembersInit());
        },
        getGroupMembersUpdate: () => {
            return dispatch(getGroupMembersUpdate());
        },
        sendCallRequest: (remote) => {
            return dispatch(sendCallRequest(remote));
        },
        acceptCallRequest: (remoteName) => {
            return dispatch(acceptCallRequest(remoteName));
        },
        rejectCall: () => {
            return dispatch(rejectCall());
        },
        cancelCall: () => {
            return dispatch(cancelCall());
        },
        endCall: () => {
            return dispatch(endCall());
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Home) ;
