/**
 * Created by front on 2017-04-28.
 */
import React from 'react';
import {connect} from 'react-redux';
//Router
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {vivarSDK} from '../api';
import {AuthRoute} from '../common';
import $ from 'jquery';

import {loginRequest} from '../actions/authentication';
import {callConnected, callDisconnected} from '../actions/sendAndAcceptCall';
import {vivarUIControlStoreInitialize} from '../actions/vivarUIControl';
import {dataChatStoreInitialize} from '../actions/dataChat';
//Container Components
import GroupJoin from './GroupJoin';
import Login from './Login';
import Home from './Home';
import NoMatch from './NoMatch';
import VivarChatMain from './VivarChatMain';

const propTypes = {
    callConnected: React.PropTypes.func,
    callDisconnected: React.PropTypes.func,
    loginStatus: React.PropTypes.bool
};

const defaultProps = {
    callConnected: () => {
    },
    callDisconnected: () => {
    },
    loginStatus:false
};

//세션 관리 Logout 처리
//TODO 자동 로그인 기능이 필요할 시 브라우저 탭 별로 세션 관리가 가능 한지 여부 파악 필요

class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            callConnected: false
        };

        this.handleCallConnected = this.handleCallConnected.bind(this);
        this.handleCallDisConnected = this.handleCallDisConnected.bind(this);
    }

    componentDidMount() {
        vivarSDK.config.localVideo = document.getElementById('local-video');
        vivarSDK.config.remoteVideo = document.getElementById('remote-video');
        console.log('vivarSDK Initialized', vivarSDK);

        vivarSDK.onLogout = function(code, reason){
            if(code === 1001 || code === 1006){
                if(window.confirm("you have lost connection to the vivar")){
                   // let groupId = this.props.groupId;
                   // this.handleGetGroupMembers(groupId);
                    window.location.replace('/');
                }
            }
        };

        vivarSDK.onError = function(error){
            console.log('[Error]: '+error);
            //do somthing
        }
    }

    componentWillMount() {
    }

    handleCallConnected() {
        console.log('onCallConnected', vivarSDK);

        this.props.callConnected(); //callConnected 시작

        this.setState({
            callConnected: true
        });
    }

    handleCallDisConnected() {
        console.log('onCallDisconnected', vivarSDK);

        this.props.callDisconnected(); //callDisconnected 시작
        this.props.vivarUIControlStoreInitialize(); //VivarUIControl 컴포넌트 store 초기화
        this.props.dataChatStoreInitialize(); //DataChat 컴포넌트 store 초기화

        this.setState({
            callConnected: false
        }, () => {
            $('#call-ended-block').css({'display': 'block'});

            setTimeout(() => {
                $('#call-ended-block').css({'display': 'none'});
            }, 2000);
        });

    }

    render() {
        const callEndedView = (
            <div id='call-ended-block' className="call_ended_block">
                <div className="wrapper">
                    <main className="content">
                        <div className="call_ended_block__profile-icon">
                            <img src="/img/user_icon.png" alt=""/>
                            <h4>{this.props.remoteName}</h4>
                        </div>
                        <div className="call_ended_block__desc">
                            <h1>{this.props.callTime}</h1>
                            <h5>Call Ended</h5>
                        </div>
                    </main>
                </div>
            </div>
        );
        return (
            <div >
                <Router>
                    <div id={'root-container'}
                         style={this.state.callConnected ? {'display': 'none'} : {'display': 'block'}}>
                        <Switch>
                            <Route exact path="/"
                                   component={ Login }
                            />
                            <AuthRoute path="/group/:groupName"
                                       groupJoinStatus={this.props.groupJoinStatus}
                                       component={ Login }
                            />
                            <Route path="/home" component={ Home }/>
                            <Route component={NoMatch}/>
                        </Switch>
                    </div>
                </Router>
                <div id={'root-container-vivar'}
                // /     style={this.state.callConnected ? {'display': 'block'} : {'display': 'none'}}
                >
                    <VivarChatMain
                        onCallConnected={this.handleCallConnected}
                        onCallDisconnected={this.handleCallDisConnected}
                    />
                </div>

                {callEndedView}
            </div>

        );
    }
}

App.propTypes = propTypes;
App.defaultProps = defaultProps;
const mapStateToProps = (state) => {
    return {
        status: state.authentication.status,
        loginStatus: state.authentication.status.isLoggedIn,
        groupJoinStatus: state.authentication.status.isJoinedGroup,
        callTime: state.sendAndAcceptCall.callInfo.callTime,
        remoteName: state.sendAndAcceptCall.callInfo.remoteName
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loginRequest: (userName, groupName, password) => {
            return dispatch(loginRequest(userName, groupName, password));
        },
        callConnected: () => {
            return dispatch(callConnected());
        },
        callDisconnected: () => {
            return dispatch(callDisconnected());
        },
        vivarUIControlStoreInitialize: () => {
            return dispatch(vivarUIControlStoreInitialize());
        },
        dataChatStoreInitialize: () => {
            return dispatch(dataChatStoreInitialize());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);