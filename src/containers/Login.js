/**
 * Created by front on 2017-04-28.
 */
import React from 'react';
import {connect} from 'react-redux';
import {Authentication} from '../components';
import {loginRequest} from '../actions/authentication';
import {CommonLayout}  from '../common';
//api
import {vivarSDK}  from '../api';
import $ from 'jquery';

//TODO 로그인 인증 처리 방식 고민
class Login extends React.Component {


    constructor(props) {
        super(props);

        this.handleLogin = this.handleLogin.bind(this);
    }

    handleLogin(userName) {
        $(".home_loading_block").css({'display': 'block'});
        $(".auth_block").css({'display': 'none'});

        const groupName = this.props.groupName;
        const password = this.props.password;

        return this.props.loginRequest(userName, groupName, password).then(
            () => {

                if (this.props.loginStatus === "SUCCESS") {

                    // create session data
                    //TODO 이 부분 어떻게 처리 할처리 고민 더 필요
                    const groupId = this.props.groupId;
                    let sessionData = {
                        'userName': userName,
                        'groupName': groupName,
                        'groupId': groupId,
                        'password': password
                    };
                    window.localStorage.setItem('sessionData', JSON.stringify(sessionData));

                    setTimeout(() => {
                        this.props.history.replace('/home');
                    }, 1000);

                    return true;
                } else {
                    $(".home_loading_block").css({'display': 'none'});
                    $(".auth_block").css({'display': 'block'});

                    alert('login fail!!!');
                    console.log('login vivarSDK',vivarSDK);
                    return false;
                }
            }
        );
    }

    render() {
        const groupName = this.props.match.params.groupName;

        const loadingView = (
            <div className="home_loading_block">
                <div className="wrapper">
                    <main className="content">
                        <h2 className="home_loading_block__title">
                            {groupName}
                        </h2>
                        <h4 className="home_loading_block__desc">Welcome {this.props.userName}</h4>
                        <div className="home_loading_block__loading-img">
                            <img className="loading_icon" src="/img/loading_icon.png" alt=""/>
                        </div>
                    </main>
                </div>
            </div>
        );
        return (
            <CommonLayout>
                <Authentication
                    mode={false}
                    onLogin={this.handleLogin}
                    groupName={groupName}
                />
                {loadingView}
            </CommonLayout>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loginStatus: state.authentication.login.status,
        groupName: state.authentication.status.groupName,
        userName: state.authentication.status.userName,
        password: state.authentication.status.password,
        groupId: state.authentication.status.groupId
    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        loginRequest: (userName, groupName, password) => {
            return dispatch(loginRequest(userName, groupName, password));
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Login) ;