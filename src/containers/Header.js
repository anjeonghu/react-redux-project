/**
 * Created by front on 2017-04-28.
 */
import React from 'react';
import {connect} from 'react-redux';
import {logout} from '../actions/authentication';
import {setTimeStamp} from '../actions/sendAndAcceptCall';
import {CallRealTimeStamp} from '../components'


const propTypes = {
    setTimeStamp: React.PropTypes.func,
};

const defaultProps = {
    setTimeStamp: ()=>{console.log("setTimeStamp is not undefined!!")},
};

//TODO 로그인 인증 처리 방식 고민
class Header extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            timer: 0
        };
        this.handleLogout = this.handleLogout.bind(this);
        this.handleTimeStamp = this.handleTimeStamp.bind(this);
    }

    componentDidMount() {

    }

    handleLogout() {

        /*테스트용*/// TODO 다구현 후 이부분 빼자
        if(window.confirm("Are you sure to leave the group?")){
            window.localStorage.removeItem('sessionData');
            this.props.logout();
            window.location.replace('/');
        }
    }

    handleTimeStamp(callTime){
        this.props.setTimeStamp(callTime);
    }

    render() {
        let headerTitle;

        if (this.props.loginStatus) {
            if (this.props.callStatus === 'CALL_CONNECTED') {
                headerTitle = 'Contected Novice ' + this.props.remoteName + ' '
            }
            else{

                headerTitle = this.props.userName + '(Expert)';
            }
        }
        return (
            <div className="header_block">
                <div className="header_block__brand-logo">
                    <img className="" src="/img/vivar_logo.png"/>
                    <div className="header_block__title">
                        {headerTitle}
                        {this.props.callStatus === 'CALL_CONNECTED' ? (<CallRealTimeStamp onCallTimeStamp={this.handleTimeStamp}/>) : ''}
                    </div>

                </div>
                <a className="header_block__logout" onClick={this.handleLogout}>
                    <img src="/img/leave_button.png" alt=""/>
                </a>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loginStatus: state.authentication.status.isLoggedIn,
        userName: state.authentication.status.userName,
        callStatus: state.sendAndAcceptCall.status,
        remoteName: state.sendAndAcceptCall.callInfo.remoteName
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => {
            return dispatch(logout());
        },
        setTimeStamp: (callTime) => {
            return dispatch(setTimeStamp(callTime));
        }
    }
};
Header.propTypes = propTypes;
Header.defaultProps = defaultProps;
export default connect(mapStateToProps, mapDispatchToProps)(Header) ;