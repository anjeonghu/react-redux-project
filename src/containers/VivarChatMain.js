/**
 * Created by front on 2017-04-28.
 */
import React from 'react';
import * as THREE from 'three';
import {connect} from 'react-redux';
import {CommonLayoutWithHeader}  from '../common';

//components
import {getGroupMembersRequest} from '../actions/getGroupMembers';
import {VideoChat, DataChat, VivarUIControl, CanvasDrawing, DisplayFileView} from '../components';

import {
    sendDrawingCaptureRequest,
    sendDrawingRequest,
    sendDirectionRequest,
    sendFileRequest,
    sendDrawingClearRequest
} from '../actions/vivarUIControl';
import {sendMessageRequest, sendMessageSuccess} from '../actions/dataChat';
import {vivarSDK} from '../api';
import $ from 'jquery';

const propTypes = {
    status: React.PropTypes.string,
    drawingStatus: React.PropTypes.string,
    capturedData: React.PropTypes.string,
    trackingPolyLines: React.PropTypes.array,
    directionStatus: React.PropTypes.string,
    isInitState: React.PropTypes.bool,
    remoteName: React.PropTypes.string,
    onCallConnected: React.PropTypes.func,
    onCallDisConnected: React.PropTypes.func
};

const defaultProps = {
    status: '',
    drawingStatus: '',
    capturedData: '',
    trackingPolyLines: [],
    directionStatus: '',
    isInitState: false,
    remoteName: '',
    onCallConnected: () => {
        console.log('onCallConnected is not defined')
    },
    onCallDisconnected: () => {
        console.log('onCallDisconnected is not defined')
    }
};

class VivarChatMain extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            drawing: {
                mode: 'INIT',
                scene: new THREE.Scene(),
                renderer: new THREE.WebGLRenderer( { alpha: true } ),
                object: {},
                canvasWidth: 100,
                canvasHeight: 100,
                context: null,
                polyLines: [],
                color: {'r': 255, 'g': 0, 'b': 0, 'a': 1},
                stickerMode: false
            },
            displayFileAndDirection: {
                mode: 'INIT',
                imageSrc: ''
            },
            isUiInit:false,
            receiveState: {
                status: false,
                stateMsg: ''
            }   // 상대 백그라운드 상태 이거나 유투브 시청 상태 등

        };
        this.handleDrawingCapture = this.handleDrawingCapture.bind(this);
        this.handleSendDrawingCapture = this.handleSendDrawingCapture.bind(this);
        this.handleSendDrawing = this.handleSendDrawing.bind(this);
        this.handleCanvasPolyLineUpdate = this.handleCanvasPolyLineUpdate.bind(this);
        this.handleSticker = this.handleSticker.bind(this);
        this.handleCanCelDrawing = this.handleCanCelDrawing.bind(this);
        this.handleUndoDrawing = this.handleUndoDrawing.bind(this);
        this.handleSendDirection = this.handleSendDirection.bind(this);
        this.handleSendFile = this.handleSendFile.bind(this);
        this.handleSendMessage = this.handleSendMessage.bind(this);
        this.handleSelectColor = this.handleSelectColor.bind(this);
        this.handleMeshUpdate = this.handleMeshUpdate.bind(this);
    }

    componentDidMount() {

        console.log('vivarChatMain component mounted!!!!!!!!!!!!!!!!');

        //canvas 객체 생성
        const canvas = document.getElementById('canvas-draw-block');
        const ctx = canvas.getContext("2d");
        // drawing 할 캔버스 에 넓이와 높이 할당
        let canvasWidth = parseInt(document.getElementById('remote-video').getAttribute('width'));
        let canvasHeight = parseInt(document.getElementById('remote-video').getAttribute('height'));
        this.setState({
            ...this.state,
            drawing: {
                ...this.state.drawing,
                canvasWidth: canvasWidth,
                canvasHeight: canvasHeight,
                canvas: canvas,
                context: ctx
            }
        });

        //통화 연결 상태 이벤트 등록
        vivarSDK.onCallConnected = function () {
            this.props.onCallConnected();
            this.setState({
                isUiInit:false,
                receiveState: {
                    status: false,
                }
            });
        }.bind(this);

        vivarSDK.onCallDisconnected = function () {
            this.props.onCallDisconnected();
            this.handleCanCelDrawing();
            this.setState({
                isUiInit:true,
            });
        }.bind(this);

        vivarSDK.onReceiveState = function (target, state) {
            if (target === 'Peer') {
                if (state === 'Background') {
                    this.setState({
                        isUiInit:true,
                        receiveState: {
                            status: true,
                            stateMsg: 'Novice ' + this.props.remoteName + ' has turned his app into a background state.'
                        }
                    });
                }
                else if (state === 'Underground') {
                    this.setState({
                        isUiInit:true,
                        receiveState: {
                            status: true,
                            stateMsg: 'Novice ' + this.props.remoteName + ' is watching the URL page.'
                        }
                    });
                }
                else if (state === 'Foreground') {
                    this.setState({
                        isUiInit:false,
                        receiveState: {
                            status: false,
                            stateMsg: ''
                        }
                    });
                }
            }
        }.bind(this);
    }

// tracking 상태일때 드로잉 캡쳐 이미지 요청 시 먼저 드로잉 클리어 해주고 호출한다.
    handleDrawingCapture() {
        if (this.state.drawing.mode === 'CAN_TRACKING') {
            this.handleCanCelDrawing().then(
                () => {
                    this.handleSendDrawingCapture();
                }
            )
        }
        else {
            this.handleSendDrawingCapture();
        }
    }

    handleSendDrawingCapture() {

        this.props.sendDrawingCaptureRequest(this.state.drawing.canvasWidth, this.state.drawing.canvasHeight, this.state.drawing.context).then(
            () => {
                if (this.props.drawingStatus === 'CAPTURE_SUCCESS') {
                    this.setState({
                        ...this.state,
                        drawing: {
                            ...this.state.drawing,
                            mode: 'CAN_DRAW'
                        }
                    }, () => {
                    });

                    document.getElementsByTagName("CANVAS")[0].style.cursor = "url('/img/cur762.cur'),auto";
                }
                else if (this.props.drawingStatus === 'CAPTURE_FAILURE') {
                    alert("통화중 상태가 아닙니다.");
                }
            }
        );
    }

    handleSelectColor(color) {

        this.setState({
            ...this.state,
            drawing: {
                ...this.state.drawing,
                color
            }
        }, () => {
        });
    }

    handleCanvasPolyLineUpdate(polyLine) {
        let polyLines = this.state.drawing.polyLines;
        polyLines.push(polyLine);
        this.setState({
            ...this.state,
            drawing: {
                ...this.state.drawing,
                polyLines: polyLines,
                stickerMode: false
            }
        });
        document.getElementsByTagName("BODY")[0].style.cursor = "auto";
        document.getElementsByTagName("CANVAS")[0].style.cursor = "url('/img/cur762.cur'),auto";

        console.log('polyLines', polyLines);
    }

    handleSticker() {
        document.getElementsByTagName("CANVAS")[0].style.cursor = "url('/img/icon_expert_draw_sticker_cursor.png'),auto";
        document.getElementsByTagName("BODY")[0].style.cursor = "url('/img/icon_expert_draw_sticker_cursor.png'),auto";

        this.setState({
            ...this.state,
            drawing: {
                ...this.state.drawing,
                stickerMode: true
            }
        });
    }

    handleSendDrawing() { // Tracking Drawing

        $('#canvas-draw').css({'display': 'none'});


        let polyLines = this.state.drawing.polyLines;


        return this.props.sendDrawingRequest(this.state.drawing.canvasWidth, this.state.drawing.canvasHeight, polyLines, this.state.drawing.context).then(
            () => {
                if (this.props.drawingStatus === 'SEND_DRAWING_SUCCESS') {
                    this.setState({
                        ...this.state,
                        drawing: {
                            ...this.state.drawing,
                            mode: 'CAN_TRACKING'
                        }
                    }, () => {
                    });
                    $('#remote-svg').css({'display': 'block'});

                    return true;
                }
                else {
                    return false;
                }
            });
    }

    handleCanCelDrawing() {

        let ctx = this.state.drawing.context;
        let width = this.state.drawing.canvasWidth;
        let height = this.state.drawing.canvasHeight;

        ctx.clearRect(0, 0, width, height);  // canvas 화면 초기화

        $('.canvas_block .add-sticker').remove(); // 스티커 초기화

        $('#canvas-draw').css({'display': 'none'}); //canvas 초기화
        $('#remote-svg').css({'display': 'none'}); //svg 초기화

        return this.props.sendDrawingClearRequest().then(
            () => {
                if (this.props.drawingStatus === 'SEND_DRAWING_CLEAR_SUCCESS') {
                    this.setState({
                        ...this.state,
                        drawing: {
                            ...this.state.drawing,
                            mode: 'INIT',
                            polyLines: [],
                            color: {'r': 255, 'g': 0, 'b': 0, 'a': 1}
                        }
                    });

                    return true;
                }
                else if (this.props.drawingStatus === 'SEND_DRAWING_CLEAR_FAILURE') {
                    return false;
                }
            }
        );
    }

    handleUndoDrawing() {

        this.setState({
            ...this.state,
            drawing: {
                ...this.state.drawing,
                polyLines: [],
                stickerMode: false
            }
        });
    }

    handleSendDirection(direction) {
        if (this.state.drawing.mode === 'CAN_TRACKING' || this.state.drawing.mode === 'CAN_DRAW') {
            this.handleCanCelDrawing(); //drawing 초기화
        }
        return this.props.sendDirectionRequest(direction).then(
            () => {
                if (this.props.directionStatus === 'SEND_DIRECTION_SUCCESS') {
                    this.setState({
                        ...this.state,
                        displayFileAndDirection: {
                            ...this.state.displayFileAndDirection,
                            mode: 'DIRECTION'
                        }
                    });

                    return true;
                }
                else if (this.props.directionStatus === 'SEND_DIRECTION_FAILURE') {
                    alert("통화중 상태가 아닙니다.");
                    return false;
                }
            }
        );
    }

    handleSendFile(file) {
        if (this.state.drawing.mode === 'CAN_TRACKING' || this.state.drawing.mode === 'CAN_DRAW') {
            this.handleCanCelDrawing();
        }

        return this.props.sendFileRequest(file).then(
            () => {
                if (this.props.fileStatus === 'SEND_FILE_SUCCESS') {

                    let messageType = this.props.file.type;
                    let regexp = /image\/.*/;
                    if (regexp.test(messageType)) {
                        let file = this.props.file;

                        let reader = new FileReader();
                        let imageSrc;

                        for (let i =1; i<= 9; i ++) {

                            for(let j=1; j<=9; j++) {
                                console.log(i + 'x' + 'j = ' + i * j)
                            }
                        }
                        reader.onload = function () {
                            imageSrc = reader.result;
                            this.props.sendMessageSuccess(file, 'image', imageSrc);

                            this.setState({
                                ...this.state,
                                displayFileAndDirection: {
                                    ...this.state.displayFileAndDirection,
                                    mode: 'FILE',
                                    imageSrc
                                }
                            });
                        }.bind(this);

                        reader.readAsDataURL(file);
                    }
                    else {
                        this.setState({
                            ...this.state,
                            displayFileAndDirection: {
                                ...this.state.displayFileAndDirection,
                                mode: 'FILE'
                            }
                        });
                        this.props.sendMessageSuccess(this.props.file, 'file', '');
                    }

                    return true;
                }
                else if (this.props.fileStatus === 'SEND_FILE_FAILURE') {
                    alert("통화중 상태가 아닙니다.");

                    return false;
                }

                return false;
            }
        );
    }

    handleSendMessage(message, messageType) {
        return this.props.sendMessageRequest(message, messageType).then(
            () => {
                if (this.props.messageStatus === 'SEND_MESSAGE_SUCCESS') {

                    return true;
                }
                else if (this.props.fileStatus === 'SEND_MESSAGE_FAILURE') {
                    alert("통화중 상태가 아닙니다.");
                    return false;
                }

                return false;
            }
        );
    }
    handleMeshUpdate(mesh) {
        this.setState({
            ...this.state,
            drawing: {
                ...this.state.drawing,
                object: mesh
            }
        });
    }

    render() {

        const loadingView = (
            <div className="capture_loading_block">
                <div className="wrapper wrapper-capture-loading">
                    <main className="content">
                        <div className="capture_loading_block-img">
                            <img className="loading_icon" src="/img/loading_icon.png" alt=""/>
                            <img className="loading_icon" src="/img/loading_icon.png" alt=""/>
                            <img className="loading_icon" src="/img/loading_icon.png" alt=""/>
                            <img className="loading_icon" src="/img/loading_icon.png" alt=""/>
                            <img className="loading_icon" src="/img/loading_icon.png" alt=""/>
                        </div>
                    </main>
                </div>
            </div>
        );
        const backgroundStateView = (
            <div className="background_disabled_block">
                <div className="row">
                    <div className="col s8">
                        <main>
                            <h6>
                                <div className="background-disabled-block__noti">
                                    {this.state.receiveState.stateMsg}
                                </div>
                            </h6>
                        </main>
                    </div>
                    <div className="col s4"></div>
                </div>
            </div>
        );
        const videoTitle = (<h6>You're watching the screen of Novice {this.props.remoteName}</h6>);
        return (
            <CommonLayoutWithHeader>
                <div className="container_video_chat col s12">
                    <div className="row">
                        <div className="col s8">
                            <main className="content">
                                <div className="container_video_chat__title">
                                    { this.props.uiStatus !== 'DRAWING' ? videoTitle : '' }
                                </div>
                                <div className="video_canvas_block">
                                    <VideoChat
                                    />
                                    <CanvasDrawing
                                        mode={this.state.drawing.mode}
                                        width={this.state.drawing.canvasWidth}
                                        height={this.state.drawing.canvasHeight}
                                        context={this.state.drawing.context}
                                        color={this.state.drawing.color}
                                        stickerMode={this.state.drawing.stickerMode}
                                        onMeshUpdate={this.handleMeshUpdate}
                                        onPolyLineUpdate={this.handleCanvasPolyLineUpdate}
                                    />
                                    {this.props.drawingStatus === 'CAPTURE_WAITING' ? loadingView : ''}

                                    <DisplayFileView
                                        mode={this.state.displayFileAndDirection.mode}
                                        imageSrc={this.state.displayFileAndDirection.imageSrc}
                                        direction={this.props.directionName}
                                        file={this.props.file}
                                    />
                                </div>
                            </main>
                            <VivarUIControl
                                isUiInit ={this.state.isUiInit}
                                stickerMode ={this.state.drawing.stickerMode}
                                onSendCapture={this.handleDrawingCapture}
                                onSendDrawing={this.handleSendDrawing}
                                onSticker={this.handleSticker}
                                onUndoDrawing={this.handleUndoDrawing}
                                onCancelDrawing={this.handleCanCelDrawing}
                                onSendDirection={this.handleSendDirection}
                                onSendFile={this.handleSendFile}
                                onSelectColor={this.handleSelectColor}
                                drawingStatus={this.props.drawingStatus}
                                capturedData={this.props.capturedData}
                                context={this.state.drawing.context}
                            />
                        </div>
                        <div className="col s4 ">
                            <DataChat
                                onSendMessage={this.handleSendMessage}
                                messageList={this.props.messageList}
                            />
                        </div>
                        {this.state.receiveState.status ? backgroundStateView : ''}
                    </div>
                </div>
            </CommonLayoutWithHeader>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        groupId: state.authentication.status.groupId,
        uiStatus: state.vivarUIControl.status,
        drawingStatus: state.vivarUIControl.drawing.status,
    //    capturedData: state.vivarUIControl.drawing.capturedData.fullContent,
        trackingPolyLines: state.vivarUIControl.drawing.polyLines,
        directionStatus: state.vivarUIControl.direction.status,
        directionName: state.vivarUIControl.direction.direction,
        file: state.vivarUIControl.file.data,
        fileStatus: state.vivarUIControl.file.status,
        messageStatus: state.dataChat.status,
        messageList: state.dataChat.messageList,
        remoteName: state.sendAndAcceptCall.callInfo.remoteName
    }

};

const mapDispatchToProps = (dispatch) => {
    return {
        getGroupMembersRequest: (groupId) => {
            return dispatch(getGroupMembersRequest(groupId));
        },
        sendDrawingCaptureRequest: (canvasWidth, canvasHeight, ctx) => {
            return dispatch(sendDrawingCaptureRequest(canvasWidth, canvasHeight, ctx));
        },
        sendDrawingRequest: (width, height, polyLines, ctx) => {
            return dispatch(sendDrawingRequest(width, height, polyLines, ctx));
        },
        sendDrawingClearRequest: () => {
            return dispatch(sendDrawingClearRequest());
        },
        sendDirectionRequest: (direction) => {
            return dispatch(sendDirectionRequest(direction));
        },
        sendFileRequest: (file) => {
            return dispatch(sendFileRequest(file));
        },
        sendMessageRequest: (message, messageType) => {
            return dispatch(sendMessageRequest(message, messageType));
        },
        sendMessageSuccess: (message, messageType, imageSrc) => {
            return dispatch(sendMessageSuccess(message, messageType, imageSrc));
        }
    }
};

VivarChatMain.propTypes = propTypes;
VivarChatMain.defaultProps = defaultProps;
export default connect(mapStateToProps, mapDispatchToProps)(VivarChatMain) ;
