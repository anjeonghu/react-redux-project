/**
 * Created by front on 2017-04-18.
 */
export const AUTH_GROUP_JOIN = "AUTH_GROUP_JOIN";
export const AUTH_GROUP_JOIN_SUCCESS = "AUTH_GROUP_JOIN_SUCCESS";
export const AUTH_GROUP_JOIN_FAILURE = "AUTH_GROUP_JOIN_FAILURE";


export const AUTH_LOGIN = "AUTH_LOGIN";
export const AUTH_LOGIN_SUCCESS = "AUTH_LOGIN_SUCCESS";
export const AUTH_LOGIN_FAILURE = "AUTH_LOGIN_FAILURE";
/*export const UPDATE_AUTH_STATUS = "UPDATE_AUTH_STATUS";*/
export const AUTH_LOGOUT = "AUTH_LOGOUT";

export const GET_GROUP_MEMBERS = "GET_GROUP_MEMBERS";
export const GET_GROUP_MEMBERS_SUCCESS = "GET_GROUP_MEMBERS_SUCCESS";
export const GET_GROUP_MEMBERS_FAILURE = "GET_GROUP_MEMBERS_FAILURE";
export const GET_GROUP_MEMBERS_UPDATE = "GET_GROUP_MEMBERS_UPDATE";


export const SEND_CALL = "SEND_CALL";
export const SEND_CALL_SUCCESS = "SEND_CALL_SUCCESS";
export const SEND_CALL_FAILURE= "SEND_CALL_FAILUER";

export const ACCEPT_CALL = "ACCEPT_CALL";
export const ACCEPT_CALL_SUCCESS = "ACCEPT_CALL_SUCCESS";
export const ACCEPT_CALL_FAILURE = "ACCEPT_CALL_FAILURE";


export const CALL_INIT = "CALL_INIT";
export const CALL_CONNECTED = "CALL_CONNECTED";
export const CALL_DISCONNECTED = "CALL_DISCONNECTED";


export const SEND_DRAWING_CAPTURE = "SEND_DRAWING_CAPTURE";
export const SEND_DRAWING_CAPTURE_SUCCESS = "SEND_DRAWING_CAPTURE_SUCCESS";
export const SEND_DRAWING_CAPTURE_FAILURE = "SEND_DRAWING_CAPTURE_FAILURE";

export const SEND_DRAWING = "SEND_DRAWING";
export const SEND_DRAWING_SUCCESS = "SEND_DRAWING_SUCCESS";
export const SEND_DRAWING_FAILURE = "SEND_DRAWING_FAILURE";

export const SEND_DRAWING_CLEAR = "SEND_DRAWING_CLEAR";
export const SEND_DRAWING_CLEAR_SUCCESS = "SEND_DRAWING_CLEAR_SUCCESS";
export const SEND_DRAWING_CLEAR_FAILURE = "SEND_DRAWING_CLEAR_FAILURE";

export const SEND_DIRECTION = "SEND_DIRECTION";
export const SEND_DIRECTION_SUCCESS = "SEND_DIRECTION_SUCCESS";
export const SEND_DIRECTION_FAILURE = "SEND_DIRECTION_FAILURE";

export const SEND_FILE = "SEND_FILE";
export const SEND_FILE_SUCCESS = "SEND_FILE_SUCCESS";
export const SEND_FILE_FAILURE = "SEND_FILE_FAILURE";

export const SEND_MESSAGE = "SEND_MESSAGE";
export const SEND_MESSAGE_SUCCESS = "SEND_MESSAGE_SUCCESS";
export const SEND_MESSAGE_FAILURE = "SEND_MESSAGE_FAILURE";

export const RECEIVE_MESSAGE_SUCCESS = "RECEIVE_MESSAGE_SUCCESS";

export const UI_STORE_RESET = "UI_STORE_RESET";
export const DATA_CHAT_STORE_RESET = "DATA_CHAT_STORE_RESET";


export const CALL_TIME_STAMP = "CALL_TIME_STAMP";

