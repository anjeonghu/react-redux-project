/**
 * Created by lg on 2017-05-03.
 */
/**
 * Created by front on 2017-04-18.
 */
import * as types from './ActionTypes';
import $ from 'jquery';
import {vivarSDK, getErrorMessage} from '../api';


export function dataChatStoreInitialize() {
    return {
        type: types.DATA_CHAT_STORE_RESET
    };
}

export function sendMessageRequest(message, messageType) { // 이벤트 핸들러 등록
    return (dispatch) => {

        dispatch(sendMessage());

        return new Promise(function (resolve, reject) {
            let errorMessage = 'sendDrawingCapture request fail';
            let resultCode = vivarSDK.sendMessage(message);

            if (resultCode !== 0) {
                errorMessage = getErrorMessage(resultCode);
                resolve(dispatch(sendMessageFailure(errorMessage)));
            }
            else{
                   resolve(dispatch(sendMessageSuccess(message, messageType, '')));
            }

        });
    };
}


export function sendMessage() {

    return {
        type: types.SEND_MESSAGE,
        status: 'WAITING',
    };
}

export function sendMessageSuccess(message, messageType, imageSrc) {
    return  {
        type: types.SEND_MESSAGE_SUCCESS,
        status: 'SEND_MESSAGE_SUCCESS',
        message,
        messageType,
        imageSrc
    };

}

export function sendMessageFailure(errorMessage) {
    return {
        type: types.SEND_MESSAGE_FAILURE,
        status: 'SEND_MESSAGE_FAILURE',
        errorMessage
    };
}
export function receiveMessageSuccess(message, messageType) {
    return {
        type: types.RECEIVE_MESSAGE_SUCCESS,
        status: 'RECEIVE_MESSAGE_SUCCESS',
        message,
        messageType
    };
}

