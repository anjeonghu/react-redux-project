/**
 * Created by lg on 2017-05-03.
 */
/**
 * Created by front on 2017-04-18.
 */
import {KJUR}  from 'jsrsasign';
import * as types from './ActionTypes';

import { getCookie ,vivarSDK, getErrorMessage} from '../api';
import { sign } from '../common';

/* initial session update */
export function updateAuthStatus(sessionData){
    return{
        type:types.UPDATE_AUTH_STATUS,
        sessionData
    };
}
/* group join */
export function groupJoinRequest(groupName, password){
    return (dispatch) => {
        //inform group join is starting
        dispatch(groupJoin());

        return Promise.resolve(dispatch(groupJoinSuccess(groupName, password)));
    };
}
export function groupJoin(){
    return{
        type:types.AUTH_GROUP_JOIN
    };
}
export function groupJoinSuccess(groupName, password){
    return{
        type:types.AUTH_GROUP_JOIN_SUCCESS,
        groupName,
        password
    };
}

export function loginRequest(userName, groupName, password){

    return (dispatch) => {
        //inform login is starting

        dispatch(login(userName));

        return new Promise(function(resolve, reject){
            //login
            let errorMessage = '';
            // let userId = groupName+'_'+userName;
            //
            // let resultCode = vivarSDK.login(userId, null, 'Expert', true);
                /*vivar event listener 등록*/

            let factoryBase64Secret = 'Zm9laSMlJDM4aTQ5MjQyM2F3cjlmaXJlMHZqbmllI1dFUnUyOTMNCnJmODl3YWVyYXdyZmEjKCRAOTMxMDM5aWUxMg==';
            var oHeader = {alg: 'HS256', typ: 'JWT'};
            var oPayload = {keyCode: userName, iat: Date.now()};  // 팩토리 테스트에서는 userId 변수의 실제 값이 초대코드가 된다.
            var sHeader = JSON.stringify(oHeader);
            var sPayload = JSON.stringify(oPayload);
            var memberToken = KJUR.jws.JWS.sign('HS256', sHeader, sPayload, {b64: factoryBase64Secret});

            let resultCode = vivarSDK.loginFactory(memberToken, true);

             if(resultCode !== 0){
                 errorMessage =  getErrorMessage(resultCode);
                resolve(dispatch(loginFailure(errorMessage)));
            }

            vivarSDK.onLogin = function (result) {
                console.log('onLogin', vivarSDK.me, result);

                if(result){
                    // vivarSDK.joinGroup(groupName, password);
                }
                else{
                    debugger;
                    errorMessage =  getErrorMessage(409);
                    resolve(dispatch(loginFailure(errorMessage)));
                }
            };
            vivarSDK.onJoinGroup = function (result, body) {
                console.log('onJoinGroup', result, body);

                if (result === true) {
                    var group = body;
                    resolve(dispatch(loginSuccess(userName, group.id, group.name)));
                }
                else{
                    errorMessage =  getErrorMessage(result);
                    resolve(dispatch(loginFailure(errorMessage)));
                }
            };

        });


    };
}
export function login(userName){
    return{
        type:types.AUTH_LOGIN,
        userName
    };
}
export function loginSuccess(userName, groupId, groupName){
    return{
        type:types.AUTH_LOGIN_SUCCESS,
        userName,
        groupId,
        groupName
    };
}
export function loginFailure(errorMessage){
    return{
        type:types.AUTH_LOGIN_FAILURE,
        errorMessage
    };
}
export function logout(){
    return{
        type:types.AUTH_LOGOUT
    };
}

