/**
 * Created by lg on 2017-05-03.
 */
/**
 * Created by front on 2017-04-18.
 */
import * as types from './ActionTypes';

import {vivarSDK, getErrorMessage} from '../api';
import {getGroupMembersUpdate} from './getGroupMembers';


export function callInit() { // 미리 등록해야되는 이벤트 핸들러 등록
    return (dispatch) => {

        return new Promise(function (resolve, reject) {
            let errorMessage = '';
            let callTimeout;
            vivarSDK.onIncomingCall = function (remoteId) {
                console.log('onIncomingCall', remoteId);
                resolve(dispatch(acceptCall(remoteId))); //accept_waiting 시작

                callTimeout = setTimeout(function () {
                    if (vivarSDK && vivarSDK.me && vivarSDK.me.state !== 'busy') {
                        console.log('시간 초과로 전화 거절');
                        resolve(dispatch(rejectCall())); //diconnect action 시작
                    }
                }, 30000);
            };

            vivarSDK.onEndCall = function(remoteId){
                clearTimeout(callTimeout);
                console.log('onEndCall', remoteId, vivarSDK);
                resolve(dispatch(endCall())); //diconnect action 시작
            };

            vivarSDK.onRejectCall = function (remoteId) {
                clearTimeout(callTimeout);
                console.log('onRejectCall', remoteId, vivarSDK);
                resolve(dispatch(rejectCall())); //diconnect action 시작
            };

        });
    };
}

export function sendCallRequest(remote) {
    return (dispatch) => {

        return new Promise(function (resolve, reject) {
            let errorMessage = '';
            //test 용

            let resultCode = vivarSDK.sendCall(remote.id);

            if (resultCode !== 0) { // 0 일때 성공
                errorMessage =  getErrorMessage(resultCode);
                resolve(dispatch(sendCallFailure(errorMessage)));

            }

            resolve(dispatch(sendCall(remote))); //send_waiting 시작
            console.log('calling result.... ',vivarSDK);
        });


    };
}

export function sendCall(remote) {
    let remoteName = remote.memberId.split('_')[1];
    return {
        type: types.SEND_CALL,
        remoteId:remote.id,
        remoteName:remoteName
    };
}

export function sendCallFailure(errorMessage) {
    return {
        type: types.SEND_CALL_FAILURE,
        errorMessage
    };
}

export function acceptCallRequest(remoteName) {
    return (dispatch) => {

        return new Promise(function (resolve, reject) {
            let errorMessage = '';
            //test 용
            let resultCode = vivarSDK.acceptCall();

            if (resultCode !== 0) { // 0 일때 성공
                errorMessage =  getErrorMessage(resultCode);
                resolve(dispatch(acceptCallFailure(errorMessage)));

            }
            else{
                resolve(dispatch(acceptCallSuccess(remoteName)));
            }

        });
    };
}

export function acceptCall(remoteId) {
    return {
        type: types.ACCEPT_CALL,
        remoteId
    };
}
export function acceptCallSuccess(remoteName) {
    return {
        type: types.ACCEPT_CALL_SUCCESS,
        remoteName
    };
}

export function acceptCallFailure(errorMessage) {
    return {
        type: types.ACCEPT_CALL_FAILURE,
        errorMessage
    };
}

export function callConnected() {
    return {
        type: types.CALL_CONNECTED,
    };
}

export function callDisconnected() {
    return {
        type: types.CALL_DISCONNECTED,
    };
}

export function rejectCall() {

    vivarSDK.rejectCall();

    return {
        type: types.CALL_DISCONNECTED,
    };
}
export function cancelCall() {

    vivarSDK.cancelCall();

    return {
        type: types.CALL_DISCONNECTED,
    };
}
export function endCall() {

    vivarSDK.endCall();

    return {
        type: types.CALL_DISCONNECTED,
    };
}
export function setTimeStamp(callTime) {

    return {
        type: types.CALL_TIME_STAMP,
        callTime:callTime
    };
}

