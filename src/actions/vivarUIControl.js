/**
 * Created by lg on 2017-05-03.
 */
/**
 * Created by front on 2017-04-18.
 */
import * as types from './ActionTypes';
import $ from 'jquery';
import cover from 'canvas-image-cover';
import {vivarSDK, getErrorMessage, canvasUtill} from '../api';


export function vivarUIControlStoreInitialize() {
    return {
        type: types.UI_STORE_RESET
    };
}

export function sendDrawingCaptureRequest(canvasWidth, canvasHeight, canvas, ctx) { // 이벤트 핸들러 등록
    return (dispatch) => {

        dispatch(sendDrawingCapture());

        let video = vivarSDK.config.remoteVideo;
        let canvas = document.getElementById('canvas-draw');

        let handleSuccess = function(stream) {
            // Attach the video stream to the video element and autoplay.
            video.srcObject = stream;
        };
        navigator.mediaDevices.getUserMedia({video: true})
            .then(handleSuccess);
        
        return new Promise(function (resolve, reject) {
            let errorMessage = 'sendDrawingCapture request fail';

            // let resultCode = vivarSDK.sendDrawingCapture();
            //
            // if (resultCode !== 0) {
            //     errorMessage = getErrorMessage(resultCode);
            //     resolve(dispatch(sendDrawingCaptureFailure(errorMessage)));
            // }

            // https://developer.mozilla.org/en-US/docs/Web/API/ImageCapture

            canvasUtill.imageCapture(canvas, vivarSDK.config.remoteVideo);
            document.getElementById('canvas-draw').style.display = 'block';

            resolve(dispatch(sendDrawingCaptureSuccess()));

            vivarSDK.onReceiveDrawingCaptureFile = function (name, contentType, fullContent) {
                console.log('onReceiveDrawingCaptureFile', name, fullContent.length);

                document.getElementById('canvas-draw').style.display = 'block';

                const image = new Image();

                image.onload = function () {
                    let pos = window.getComputedStyle(vivarSDK.config.remoteVideo).getPropertyValue('object-position').split(' '); // % 포지션만 지원
                    canvasUtill.imageDrawingCover(ctx, image, 0, 0, canvasWidth, canvasHeight, pos);
                };
                image.src = "data:image/png;base64," + fullContent; //captured된 이미지를 display한다.

                let capturedData = {name, contentType, fullContent};
                resolve(dispatch(sendDrawingCaptureSuccess(capturedData)));
            };
        });
    };
}


export function sendDrawingCapture() {
    return {
        type: types.SEND_DRAWING_CAPTURE,
        status: 'CAPTURE_WAITING',
    };
}

export function sendDrawingCaptureSuccess(capturedData) {
    return {
        type: types.SEND_DRAWING_CAPTURE_SUCCESS,
        status: 'CAPTURE_SUCCESS',
        capturedData
    };
}

export function sendDrawingCaptureFailure(errorMessage) {
    return {
        type: types.SEND_DRAWING_CAPTURE_FAILURE,
        status: 'CAPTURE_FAILURE',
        errorMessage
    };
}
export function sendDrawingRequest(canvasWidth, canvasHeight, polyLines, ctx) { // 이벤트 핸들러 등록
    return (dispatch) => {

        dispatch(sendDrawing());

        return new Promise(function (resolve, reject) {
            let errorMessage = 'sendDrawing request fail';
            let resultCode;

            let pos = window.getComputedStyle(vivarSDK.config.remoteVideo).getPropertyValue('object-position').split(' '); // % 포지션만 지원

            let relVideoSizePosition = vivarSDK.helper.getRelativeVideoSizePosition(false, canvasWidth, canvasHeight, parseInt(pos[0]), parseInt(pos[1]));

            resultCode = vivarSDK.sendDrawing(relVideoSizePosition.width, relVideoSizePosition.height, polyLines);

            if (resultCode !== 0) {
                errorMessage = getErrorMessage(resultCode);
                resolve(dispatch(sendDrawingFailure(errorMessage)));
            }
            let svgContainer = document.getElementById('remote-svg');


            vivarSDK.onReceiveDrawingTracking = function (result, polyLines) {
                console.log('onReceiveDrawingTracking', result, polyLines);
                $('.canvas_block .add-sticker').remove();

                svgContainer.innerHTML = '';
                if (result === true) {

                    for (let index = 0; index < polyLines.length; index++) {
                        let svgPoints ='';
                        let polylineEle = document.createElementNS('http://www.w3.org/2000/svg', 'polyline');
                        svgPoints = polylineEle.getAttribute('points') || '';

                        let polyLine = polyLines[index].points;
                        let color = polyLines[index].color;

                        polylineEle.setAttribute('style', 'fill:none;stroke-width:5;stroke:rgba(' + color.r + ',' + color.g + ',' + color.b + ',' + color.a + ')');

                        for (let index2 = 0; index2 < polyLine.length; index2++) {

                            let lines = polyLine[index2];
                            let x = lines.x;
                            let y = lines.y;

                            if (polyLines[index].uuid !== '' && polyLines[index].uuid === 'PointingSticker') {
                                $('<img/>').attr({
                                    'class': 'add-sticker',
                                    'src': '/img/icon_expert_draw_sticker_cursor.png'
                                })
                                    .appendTo('.canvas_block').css({
                                    'position': 'absolute',
                                    'left': x,
                                    'top': y,
                                    'z-index': '999'
                                }); // 제이쿼리 안쓰려다 귀찮아서 이때 부터 씀
                            }
                            else {

                                svgPoints += (x + ',' + y + ' ');
                                polylineEle.setAttribute('points', svgPoints);

                                svgContainer.appendChild(polylineEle);
                            }
                        };

                    };

                    resolve(dispatch(sendDrawingSuccess(polyLines)));
                }
                else {
                    errorMessage = getErrorMessage(result);
                    resolve(dispatch(sendDrawingFailure(errorMessage)));
                }
            };
        });
    };
}


export function sendDrawing() {

    return {
        type: types.SEND_DRAWING,
        status: 'SEND_DRAWING_WAITING',
    };
}

export function sendDrawingSuccess(polyLines) {
    return {
        type: types.SEND_DRAWING_SUCCESS,
        status: 'SEND_DRAWING_SUCCESS',
        polyLines
    };
}

export function sendDrawingFailure(errorMessage) {
    return {
        type: types.SEND_DRAWING_FAILURE,
        status: 'SEND_DRAWING_FAILURE',
        errorMessage
    };
}

export function sendDrawingClearRequest() { // 이벤트 핸들러 등록
    return (dispatch) => {

        return new Promise(function (resolve, reject) {
            let errorMessage = '';

            let resultCode = vivarSDK.sendDrawingClear();

            if (resultCode !== 0) {
                errorMessage = getErrorMessage(resultCode);
                resolve(dispatch(sendDrawingClearFailure(errorMessage)));
            }

            resolve(dispatch(sendDrawingClearSuccess()));
        });
    };
}
export function sendDrawingClearSuccess() {
    return {
        type: types.SEND_DRAWING_CLEAR_SUCCESS,
        status: 'SEND_DRAWING_CLEAR_SUCCESS'
    };
}
export function sendDrawingClearFailure(errorMessage) {
    return {
        type: types.SEND_DRAWING_CLEAR_FAILURE,
        status: 'SEND_DRAWING_CLEAR_FAILURE',
        errorMessage
    };
}

export function sendDirectionRequest(direction) { // 이벤트 핸들러 등록
    return (dispatch) => {

        dispatch(sendDirection());

        return new Promise(function (resolve, reject) {
            let errorMessage = '';

            let resultCode = vivarSDK.sendDirection(direction);

            if (resultCode !== 0) {
                errorMessage = getErrorMessage(resultCode);
                resolve(dispatch(sendDirectionFailure(errorMessage)));
            }
            else {
                resolve(dispatch(sendDirectionSuccess(direction)));
            }

        });
    };
}


export function sendDirection() {

    return {
        type: types.SEND_DIRECTION,
        status: 'SEND_DIRECTION',
    };
}

export function sendDirectionSuccess(direction) {
    return {
        type: types.SEND_DIRECTION_SUCCESS,
        status: 'SEND_DIRECTION_SUCCESS',
        direction
    };
}

export function sendDirectionFailure(errorMessage) {
    return {
        type: types.SEND_DIRECTION_FAILURE,
        status: 'SEND_DIRECTION_FAILURE',
        errorMessage
    };
}

export function sendFileRequest(file) { // 이벤트 핸들러 등록
    return (dispatch) => {

        dispatch(sendFile());

        return new Promise(function (resolve, reject) {
            let errorMessage = '';

            let resultCode = vivarSDK.sendFile(file);

            if (resultCode !== 0) {
                errorMessage = getErrorMessage(resultCode);
                resolve(dispatch(sendFileFailure(errorMessage)));
            }
            else {
                resolve(dispatch(sendFileSuccess(file)));
            }


            /* vivarSDK.onReceiveFile = function (name, contentType, fullContent) {
             let file = {name, contentType, fullContent};
             };*/
        });
    };
}


export function sendFile() {

    return {
        type: types.SEND_FILE,
        status: 'SEND_FILE_WAITING',
    };
}

export function sendFileSuccess(file) {
    return {
        type: types.SEND_FILE_SUCCESS,
        status: 'SEND_FILE_SUCCESS',
        file
    };
}

export function sendFileFailure(errorMessage) {
    return {
        type: types.SEND_FILE_FAILURE,
        status: 'SEND_FILE_FAILURE',
        errorMessage
    };
}


