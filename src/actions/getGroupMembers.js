/**
 * Created by lg on 2017-05-03.
 */
/**
 * Created by front on 2017-04-18.
 */
import * as types from './ActionTypes';

import {vivarSDK, getErrorMessage} from '../api';

export function groupMembersInit() { //미리 등록해야되는 이벤트 핸들러 등록
    return (dispatch) => {

        return new Promise(function (resolve, reject) {
            let errorMessage = '';

            //group에 변경사항이 있을때 호출되는 이벤트 핸들러
            vivarSDK.onNotifyJoinGroup = function (groupId, member) {
                console.log('onNotifyUpdateMember', member);

                let result = vivarSDK.getGroupMembers(groupId);

                if (result !== 0) { // 0 일때 성공
                    errorMessage =  getErrorMessage(result);
                    resolve(dispatch(getGroupMembersFailure(errorMessage)));
                }
            };

            //다른 멤버가 그룹을 떠났을때
            vivarSDK.onNotifyLeaveGroup = function(groupId, member){
                console.log('onNotifyLeaveGroup',member);
                let result = vivarSDK.getGroupMembers(groupId);

                if (result !== 0) { // 0 일때 성공
                    errorMessage =  getErrorMessage(result);
                    resolve(dispatch(getGroupMembersFailure(errorMessage)));
                }
            };
            //다른 멤버의 상태가 병경되었을때 발생
            vivarSDK.onNotifyUpdateState = function(groupId,member){
                debugger;
                console.log('onNotifyUpdateState',member);

                console.log('onNotifyUpdateState', groupId, member);
                let result = vivarSDK.getGroupMembers(groupId);

                if (result !== 0) { // 0 일때 성공
                    errorMessage =  getErrorMessage(result);
                    resolve(dispatch(getGroupMembersFailure(errorMessage)));
                }
            };
            // 자신이 그룹을 떠날때
            vivarSDK.onLeaveGroup = function(result, group){
                if (result === true) {
                    console.log('onleaveGroup',group);
                    //do something
                }
                else {
                    //do something
                }
            };
        });
    };
}

export function getGroupMembersRequest(groupId) {
    return (dispatch) => {
        //inform login is starting

        dispatch(getGroupMembers());

        return new Promise(function (resolve, reject) {
            //login
            let errorMessage = '';
            let resultCode;

            resultCode = vivarSDK.getGroupMembers(groupId);

            /*vivar event listener 등록*/
            vivarSDK.onGetGroupMembers = function (result, body) {
                console.log('onGetGroupMembers',vivarSDK.me,result, body);
                if (result === true) {

                    const groupId = body.groupId;
                    const memberList = body.members;
                    console.log('memberList',memberList);
                    resolve(dispatch(getGroupMembersSuccess(groupId, memberList)));
                }
                else {
                    errorMessage =  getErrorMessage(result);
                    resolve(dispatch(getGroupMembersFailure(errorMessage)));
                }
            };

            console.log('getGroupMembers',vivarSDK.me, vivarSDK);

            if (resultCode !== 0) { // 0 일때 성공
                errorMessage =  getErrorMessage(resultCode);
                resolve(dispatch(getGroupMembersFailure(errorMessage)));
            }

        });


    };
}

export function getGroupMembers() {
    return {
        type: types.GET_GROUP_MEMBERS
    };
}
export function getGroupMembersSuccess(groupId, memberList) {
    return {
        type: types.GET_GROUP_MEMBERS_SUCCESS,
        memberList:memberList
    };
}
export function getGroupMembersFailure(errorMessage) {
    return {
        type: types.GET_GROUP_MEMBERS_FAILURE,
        errorMessage
    };
}
export function getGroupMembersUpdate(groupId, member) {
    return {
        type: types.GET_GROUP_MEMBERS_UPDATE,
        member: member
    };
}
