/**
 * Created by front on 2017-05-12.
 */
import React from 'react';
import './css/dataChat.css';
import vivarSDK from '../api/vivarSDK';
import $ from 'jquery';

import ChatMessage from './ChatMessage';

const propTypes = {
    messageList: React.PropTypes.array,
    onSendMessage: React.PropTypes.func
};

const defaultProps = {};

class DataChat extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            textAreaMessage:''
        };

        this.onSendMessage = this.onSendMessage.bind(this);
        this.onChange = this.onChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        let update = JSON.stringify(this.props) !== JSON.stringify(nextProps);
        return update;
    }

    componentDidUpdate(){
        let scrollHeight = $('.chat_block__messages-content').prop('scrollHeight');
        $('.chat_block__messages-content').scrollTop(scrollHeight);// auto scrolling
    }


    handleKeyPress(e) {
        if (e.charCode == 13) {
        e.preventDefault();
            this.onSendMessage();
        }
    }

    onSendMessage() {

        if(this.state.textAreaMessage === ''){
            return;
        }
        let message = this.state.textAreaMessage;
        let messageType = 'text';

        if (message.test(/.*((http(s?)):\/\/).*/gi)) {
            messageType = 'link';
        }
        this.props.onSendMessage(message, messageType).then(
            (result) => {

                this.setState({
                    textAreaMessage:''
                });

                $('#sendDataMessage').val('');
            }
        );
    }

    onChange(e){
        this.setState({
            textAreaMessage:e.target.value
        });
    }

    render() {

        let messagesView = (<div></div>);
        let messages = this.props.messageList;

        if(messages.length>0){
            messagesView = messages.map((msg, index)=>{
                return (
                    <ChatMessage key={index} message={msg.message} messageType={msg.messageType} imageSrc={msg.imageSrc}/>
                );
            });
        }


        return (
            <main className="content">
                <div className="chat_block">
                    <div className="chat_block__messages">
                        <div className="chat_block__messages-content">
                            <div id='send-data' className="chat_block__send-data">
                                {messagesView}
                            </div>
                        </div>
                    </div>
                    <div className="chat_block__input-box">
                        <textarea type="text"
                                  rows="3"
                                  id='sendDataMessage'
                                  className="message-input"
                                  placeholder="Type message..."
                                  onChange={this.onChange}
                                  onKeyPress={this.handleKeyPress}>
                        </textarea>
                        <img id="sendDatabtn" className="message-submit" src="/img/text_enter_button_nor.png"
                             onClick={this.onSendMessage}/>
                    </div>
                </div>
            </main>
        );
    }

}

DataChat.propTypes = propTypes;
DataChat.defaultProps = defaultProps;

export default DataChat;