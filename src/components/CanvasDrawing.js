/**
 * Created by front on 2017-05-12.
 */
import React from 'react';
import $ from 'jquery';

import './css/canvasDrawing.css';
import * as THREE from 'three';
import {vivarSDK, canvasUtill} from '../api';

const propTypes = {
    data: React.PropTypes.string,
    width: React.PropTypes.number,
    height: React.PropTypes.number,
    item: React.PropTypes.object,
    context: React.PropTypes.object,
    color: React.PropTypes.object,
    mode: React.PropTypes.string,
    stickerMode: React.PropTypes.bool,
    onPolyLineUpdate: React.PropTypes.func,
    onMeshUpdate: React.PropTypes.func
};

const defaultProps = {
    data: '',
    width: 100,
    height: 100,
    item: null,
    context: null,
    mode: '',
    stickerMode: false,
    onPolyLineUpdate: () => {
        'onPolyLineUpdate is not defined'
    }
};

class CanvasDrawing extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            mousePressed: false,
            context: null,
            startX: 0,
            startY: 0,
            mostLeft: 0,
            mostRight: 0,
            mostTop: 0,
            mostBottom: 0,
            points: [],
            scene: new THREE.Scene(),
            renderer: new THREE.WebGLRenderer({alpha: true}),
            line: null,
            camera: null,
            object: {},
        };

        this.mouseDown = this.mouseDown.bind(this);
        this.mouseMove = this.mouseMove.bind(this);
        this.mouseUp = this.mouseUp.bind(this);
        this.makePolyLineObj = this.makePolyLineObj.bind(this);
    }

    componentDidMount() {
        let canvasWidth = parseInt(document.getElementById('remote-video').getAttribute('width'));
        let canvasHeight = parseInt(document.getElementById('remote-video').getAttribute('height'));
        let camera = new THREE.PerspectiveCamera(
            60,
            canvasWidth / canvasHeight,
            0.3,
            1000
        );
        camera.position.set(0, 0, 0.5);
        //camera.lookAt(new THREE.Vector3() );
        camera.lookAt(new THREE.Vector3());

        this.state.renderer.setClearColor(new THREE.Color(0xEEEEEE), 0);
        this.state.renderer.setSize(canvasWidth, canvasHeight);
        document.getElementById('canvas-gl-block').appendChild(this.state.renderer.domElement);

        this.state.renderer.render(this.state.scene, camera);
        // create a scene, that will hold all our elements such as objects, cameras and lights.
        let canvas = document.getElementById('canvas-draw-block');
        let context = canvas.getContext('2d');
        this.setState({
            context,
            camera
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        let update = JSON.stringify(this.props) !== JSON.stringify(nextProps);
        return update;
    }

    canvasGlrender() {
        this.state.renderer.render(this.state.scene, this.state.camera);
    }

    mouseDown(event) {

        if (this.props.stickerMode) {
            return false;
        }
        let x = event.pageX - $("#canvas-draw-block").offset().left;
        let y = event.pageY - $("#canvas-draw-block").offset().top;

        // let rect = document.getElementById('canvas-gl-draw').getBoundingClientRect(); // 전체 요소를 포함 하는 경계 박스
        // let x = ( event.nativeEvent.offsetX / rect.width ) * 2 - 1;
        // let y = -( event.nativeEvent.offsetY / rect.height ) * 2 + 1;
        var point = {};
        console.log('mouseDown', event.pageX, event.pageY, $("#canvas-draw").offset());


        this.setState({
            mousePressed: true,
            mostLeft: x,
            mostRight: x,
            mostTop: y,
            mostBottom: y
        }, () => { // setState callback 함수
            // this.drawLine(x, y, false, this.props.color);
            this.drawLine(x, y, false, 'red');
        });

    };

    mouseMove(event) {

        if (this.props.stickerMode) {
            return false;
        }

        // let x = event.pageX - $("#canvas-draw").offset().left;
        // let y = event.pageY - $("#canvas-draw").offset().top;


        let x = event.pageX - $("#canvas-draw-block").offset().left;
        let y = event.pageY - $("#canvas-draw-block").offset().top;


        // let rect = document.getElementById('canvas-gl-draw').getBoundingClientRect(); // 전체 요소를 포함 하는 경계 박스
        // let x = ( event.nativeEvent.offsetX / rect.width ) * 2 - 1;
        // let y = -( event.nativeEvent.offsetY / rect.height ) * 2 + 1;
        var point = {};
        if (this.state.mousePressed) {
            let left = this.state.mostLeft,
                right = this.state.mostRight,
                top = this.state.mostTop,
                bottom = this.state.mostBottom;
            if (x < this.state.mostLeft) left = x;
            if (x > this.state.mostRight) right = x;
            if (y < this.state.mostTop) top = y;
            if (y > this.state.mostBottom) bottom = y;

            point = canvasUtill.getRealCoordinate({x, y});

            // end gl draw
            let points = this.state.points.slice();
            points.push(point);
            this.setState({
                points: points,
                mostLeft: left,
                mostRight: right,
                mostTop: top,
                mostBottom: bottom,
            });
            this.drawLine(x, y, true, this.props.color);
        } else {
            this.setState({
                moveCount: 0
            });
        }


    };

    mouseUp(event) {

        let x;
        let y;

        x = event.pageX - $("#canvas-draw-block").offset().left;
        y = event.pageY - $("#canvas-draw-block").offset().top;


        console.log(this)
        //let point = {x, y};
        let point = canvasUtill.getRealCoordinate({x, y});

        let points = this.state.points.slice();
        points.push(point);

        if (this.props.stickerMode) {
            this.setState({
                    points: points
                },
                () => { // setState callback 함수
                    let polyLine = this.makePolyLineObj('PointingSticker');
                    this.props.onPolyLineUpdate(polyLine);

                    this.setState({
                        points: [] // points 초기화
                    });
                }
            );

            $('<img/>').attr({'class': 'add-sticker', 'src': '/img/icon_expert_draw_sticker_cursor.png'})
                .appendTo('.canvas_block').css({'position': 'absolute', 'left': x, 'top': y, 'z-index': '999'});
        }
        else {
            //const ctx = this.props.context;
            const ctx = this.state.context;
            ctx.save();
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            let left = Math.max(this.state.mostLeft - 5, 0);
            let right = Math.min(this.state.mostRight + 5, ctx.canvas.width);
            let top = Math.max(this.state.mostTop - 5, 0);
            let bottom = Math.min(this.state.mostBottom + 5, ctx.canvas.height);

            let width = right - left;
            let height = bottom - top;

            var imgData = ctx.getImageData(left, top, width, height);
          //  ctx.putImageData(imgData, 10, 70);
            ctx.restore();
            //let data = new Uint8Array(imgData.data);
            var texture = new THREE.DataTexture(new Uint8Array(imgData.data), imgData.width, imgData.height, THREE.RGBAFormat);
            texture.needsUpdate = true;
            texture.magFilter = THREE.NearestFilter;
            texture.flipY = true;

            console.log(imgData);
            console.log('clipping', ctx);
            //let material = new THREE.MeshBasicMaterial( {map: texture, side:THREE.DoubleSide } );
            let material = new THREE.MeshBasicMaterial({side: THREE.FrontSide});
            material.transparent = true;
            material.map = texture;
            //material.alphaMap.magFilter = THREE.NearestFilter;
            //
            let mesh = new THREE.Mesh(new THREE.PlaneGeometry(imgData.width, imgData.height, imgData.width, imgData.height), material);


            // let geometry = new THREE.BoxBufferGeometry( 10, 10, 10, 10, 10, 10 ); //  width, height, depth, widthSegments, heightSegments, depthSegments
            // let mesh = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial({ color: 0x00D8FF, side: THREE.DoubleSide }) );

            mesh.position.set(0, 0, -200);
            //   mesh.rotation.x = (180 / Math.PI) * 90;
            console.log('mesh', mesh);

            this.state.scene.add(mesh);
            this.state.renderer.render(this.state.scene, this.state.camera);
            this.setState({
                    ...this.state,
                    mousePressed: false,
                    points: points,
                    moveCount: 0
                },
                () => {
                    const polyLine = this.makePolyLineObj();
                    this.props.onPolyLineUpdate(polyLine);
                    this.props.onMeshUpdate(mesh);

                    this.setState({
                        points: [] // points 초기화
                    });
                });

        }

    };

    mouseLeave(event) {
        if (this.props.stickerMode) {
            return false;
        }
        console.log('mouseLeave');
        this.setState({
            mousePressed: false
        });
    };

    drawLine(x, y, isDown, color = {'r': '255', 'g': '0', 'b': '0', 'a': '1'}) {

        let rgba = 'rgba(' + color.r + ',' + color.g + ',' + color.b + ',' + color.a + ')';

        if (isDown) {
            // const ctx = this.props.context;
            const ctx = this.state.context;

            ctx.save();
            ctx.beginPath();
            ctx.strokeStyle = rgba;
            ctx.lineWidth = '7';
            ctx.lineJoin = 'round';
            ctx.lineCap = 'round';
            ctx.moveTo(this.state.startX, this.state.startY);
            ctx.lineTo(x, y);
            ctx.closePath();
            ctx.stroke();
            ctx.restore();
        }

        this.setState({
            startX: x,
            startY: y
        });
    };

    makePolyLineObj(uuid = '') {
        let color = this.props.color;
        let points = this.state.points;
        const polyLine = {uuid, color, points};

        return polyLine;
    }

    render() {
        /*        if(this.props.stickerMode){
         document.getElementsByTagName("BODY")[0].style.cursor = "url('/img/icon_expert_draw_sticker_cursor.png'),auto";
         }
         else{
         document.getElementsByTagName("CANVAS")[0].style.cursor = "url('/img/cur762.cur'),auto";
         document.getElementsByTagName("BODY")[0].style.cursor = "auto";
         }*/
        let preventEvent = false;
        let className = '';
        if (this.props.mode === 'CAN_TRACKING') {
            preventEvent = true;
            className = 'canvas_block__canvas--tracking';
        }
        const captureView = (
            <div>
                <canvas
                    className={`canvas_block__canvas ${className}`}
                    id='canvas-capture'
                    // onMouseDown={ preventEvent? ()=> {return false}: this.mouseDown }
                    // onMouseMove={ preventEvent? ()=> {return false}: this.mouseMove }
                    // onMouseUp={ preventEvent? ()=> {return false}: this.mouseUp }
                    width={ this.props.width}
                    height={this.props.height}
                    style={{'display': 'none'}}
                />
                <svg id='remote-svg' width={ this.props.width} height={this.props.height} className="draw_svg"/>
            </div>
        );
        const drawView = (
            <div>
                <canvas
                    className={`canvas_block__canvas ${className}`}
                    id="canvas-draw-block"
                    onMouseDown={ preventEvent ? () => {
                        return false
                    } : this.mouseDown }
                    onMouseMove={ preventEvent ? () => {
                        return false
                    } : this.mouseMove }
                    onMouseUp={ preventEvent ? () => {
                        return false
                    } : this.mouseUp }
                    // width= { this.props.width}
                    // height={this.props.height}
                    width= { 844}
                    height={ 422}
                    style={{'background-color': 'transparent'}}
                />
            </div>
        );

        const WebGlView = (
                <div
                    id="canvas-gl-block"
                    className={`canvas_block__canvas ${className}`}
                    // width= { this.props.width}
                    // height={this.props.height}
                    width= { 844}
                    height={ 422}
                    style={{'display': 'block'}}
                />
        );


        return (
            <div className="canvas_block">
                {drawView}
                {captureView}
                {WebGlView}
            </div>
        );
    }
}
;

CanvasDrawing.propTypes = propTypes;
CanvasDrawing.defaultProps = defaultProps;

export default CanvasDrawing;