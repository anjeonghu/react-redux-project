/**
 * Created by front on 2017-05-12.
 */
import React from 'react';
import vivarSDK from '../api/vivarSDK';

const propTypes = {
    onCallTimeStamp: React.PropTypes.func
};

const defaultProps = {
    onCallTimeStamp: () => {
        console.log("onCallTimeStamp is not undefined")
    }
};

class CallRealTimeStamp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            timer: 0
        };
        this.toHourMinSec = this.toHourMinSec.bind(this);
        this.handleCallTimer = this.handleCallTimer.bind(this);
        this.toHourMinSec = this.toHourMinSec.bind(this);
    }

    componentDidMount() {
        this.timer = window.setInterval(this.handleCallTimer, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
        this.props.onCallTimeStamp(this.toHourMinSec(this.state.timer));
    }

    handleCallTimer() {
        this.setState({
            timer: this.state.timer + 1
        });
    }

    toHourMinSec(t) {
        var hour = "00";
        var min = "00"
        var sec = "00"

        // 정수로부터 남은 시, 분, 초 단위 계산
        hour = Math.floor(t / 3600);

        min = Math.floor((t - (hour * 3600)) / 60);

        sec = t - (hour * 3600) - (min * 60);

        // hh:mm:ss 형태를 유지하기 위해 한자리 수일 때 0 추가

        if (hour < 10) hour = "0" + hour;

        if (min < 10) min = "0" + min;

        if (sec < 10) sec = "0" + sec;
        return hour + ":" + min + ":" + sec;
    }

    render() {
        return (
            <div style={{'display':'inline-block'}}>
                <span>
                    {this.toHourMinSec(this.state.timer)}
                </span>
            </div>
        );
    }
}

CallRealTimeStamp.propTypes = propTypes;
CallRealTimeStamp.defaultProps = defaultProps;

export default CallRealTimeStamp;