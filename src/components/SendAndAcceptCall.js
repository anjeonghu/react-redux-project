/**
 * Created by front on 2017-05-10.
 */
import React from 'react';
import './css/sendAndAcceptCall.css';

const propTypes = {
    data: React.PropTypes.object,
    onCancelCall: React.PropTypes.func
};

const defaultProps = {
    data: {},
    onCancelCall: () => {
        console.log('onCancelCall is not defined!!')
    }
};

class SendAndAcceptCall extends React.Component {
    constructor(props) {
        super(props);

        this.onAcceptCall = this.onAcceptCall.bind(this);
        this.onRejectCall = this.onRejectCall.bind(this);
        this.onCanCelCall = this.onCanCelCall.bind(this);
    }

    onAcceptCall() {
        this.props.onAcceptCall();
    }

    onRejectCall() {
        this.props.onRejectCall();
    }

    onCanCelCall() {
        this.props.onCancelCall();
    }

    render() {
        let remoteName;

        if(this.props.mode === 'ACCEPT_WAITING'||this.props.mode === 'SEND_WAITING'){
            remoteName = this.props.memberList[this.props.remoteId].memberId;
            remoteName = remoteName.split('_')[1];
        }

        const defaultView = (
            <div className="call_block">
                <span className="call_block__title">How you can help a novice</span>
                <div className="call_block__img-description center">
                    <img className="" src="/img/expert_img.png"/>
                </div>
                <div className="call_block__description">
                    You can help a novice more effectively by AR drawing as if it's on the real object.<br/>
                    Also you can support him with multi-content such as 3D, image and text.
                </div>
            </div>
        );

        const sendCallView = (
            <div className="send_call_block">
                <div className="send_call_block__icon">
                    <div className="send_call_block__profile-icon">
                        <img src="/img/user_icon.png" alt=""/>
                    </div>
                    <h4>{remoteName}</h4>
                    <div>
                        <h5>Calling...</h5>
                    </div>
                    <div className="send_call_block__btn">
                        <img className="end-call" src="/img/videocall cancel_nor.png" onClick={this.onCanCelCall} style={{'margin':'0'}}/>
                    </div>
                </div>
            </div>
        );

        const acceptCallView = (
            <div className="accept_call_block">
                <div className="accept_call_block__icon">
                    <div className="accept_call_block__profile-icon">
                        <img src="/img/user_icon.png" alt=""/>
                    </div>
                    <h4>{remoteName}</h4>
                    <div>
                        <h5>Incoming call</h5>
                    </div>
                    <div className="accept_call_block__btn">
                        <img
                            className="accept-call"
                            src="/img/videocall accept_nor.png"
                           onClick={this.onAcceptCall}
                            style={{'margin':'82px 43px'}}
                        />
                        <img
                            className="end-call"
                            src="/img/videocall cancel_nor.png"
                            onClick={this.onRejectCall}
                            style={{'margin':'82px 43px'}}
                        />
                    </div>
                </div>
            </div>
        );

        var viewMode = defaultView;

        if (this.props.mode === 'SEND_WAITING') {
            viewMode = sendCallView;
        }
        else if (this.props.mode === 'ACCEPT_WAITING') {
            viewMode = acceptCallView;
        }

        return (
            <div className={`send_accept_call_main ${this.props.mode} col s9`}>
                <div className="row">
                    <main className="content">
                        {viewMode}
                    </main>
                </div>
            </div>
        )

    }
}

SendAndAcceptCall.propTypes = propTypes;
SendAndAcceptCall.defaultProps = defaultProps;

export default SendAndAcceptCall;