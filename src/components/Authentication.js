/**
 * Created by front on 2017-04-26.
 */
import React from 'react';
import './css/authentication.css';
import $ from 'jquery';

const propTypes = {
    mode: React.PropTypes.bool,
    onGroupJoin: React.PropTypes.func,
    onLogin: React.PropTypes.func
};

const defaultProps = {
    mode: true,
    onGroupJoin: (groupName) => {
        console.log("group join function not defined")
    },
    onLogin: (id) => {
        console.log("login function not defined")
    }
};


class Authentication extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            groupName: "",
            password: "",
            userName: "",
            visible: false,
            invalid: false,
            isDuplicatedId:false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleGroupJoin = this.handleGroupJoin.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleVisible = this.handleVisible.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleInvalidCheck = this.handleInvalidCheck.bind(this);
    }

    handleKeyPress(e) {

        if (e.charCode == 13) {
            e.preventDefault();
            if (this.props.mode) {
                this.handleGroupJoin();
            }
            else {
                this.handleLogin();
            }
        }
    }

    handleInvalidCheck() {
        let regex = /^[a-z0-9_]{3,21}$/gi;
        let groupName = $('#input-groupName').val() || '';
        let userName = $('#input-userName').val() || '';

        if (regex.test(groupName) || regex.test(userName)){
            this.setState({
                invalid: false
            });
        }
        else {
            this.setState({
                invalid: true
            });
        }
    }


    handleChange(e) {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState,
            () => {
                this.handleInvalidCheck();
            }
        );
    };

    handleGroupJoin() {

        let groupName = this.state.groupName;
        let password = this.state.password;

        if (groupName === '' || this.state.invalid === true) {
            this.setState({
                invalid: true
            });

            return false;
        }

        this.props.onGroupJoin(groupName, password).then(
            (success) => {
                if (!success) {
                    this.setState({
                        password: ''
                    });

                }
                //  window.location.replace('/group/'+groupName);
            }
        );
    }

    handleLogin() {

        let userName = this.state.userName;

        if (userName === '' || this.state.invalid === true) {
            this.setState({
                invalid: true
            });

            return false;
        }

        this.props.onLogin(userName).then(
            (success) => {
                if (success) {
                    this.setState({
                        userName: ''
                    });
                }
                else{
                    this.setState({
                        isDuplicatedId: true
                    });
                }
            }
        );
    }

    handleVisible() {
        this.setState({
            visible: !this.state.visible
        });
    }


    render() {
        let type = this.state.visible ? 'text' : 'password';
        let activeClass = this.state.visible ? 'auth_block__icon--active' : '';
        let invalidView = (
            <p className="">
                You can enter only upper and lower case letters,<br/>
                numbers and underscore. (4 ~ 20 characters)
            </p>
        );
        let isDuplicatedIdView = (
            <p className="">
                It is already used, Please use another ID
            </p>
        );

        const groupJoinView = (
            <div>
                <header><img src="/img/viva_logo_big.png" alt="logo"/></header>
                <span className="auth_block__title">Enter a Group Name to Open or Join</span>
                <form className="auth_block__form">
                    <div className="auth_block__input-filed">
                        <label htmlFor="icon_prefix">Group</label>
                        <input
                            id="input-groupName"
                            name="groupName"
                            type='text'
                            className="validate"
                            onChange={this.handleChange}
                            value={this.state.groupName}
                            onKeyPress={this.handleKeyPress}
                            autoFocus
                        />

                    </div>
                    <div className="auth_block__invalid-message">
                        {this.state.invalid ? invalidView : ''}
                    </div>
                    <div className="auth_block__input-filed">
                        <label htmlFor="">Password</label>
                        <input
                            id="input-groupName"
                            name="password"
                            type={type}
                            className="validate"
                            onChange={this.handleChange}
                            value={this.state.password}
                            onKeyPress={this.handleKeyPress}
                        />
                        <img src="/img/notice_eye_icon.png"
                             alt=""
                             className={`auth_block__icon ${activeClass} `}
                             onClick={this.handleVisible}
                             ></img>
                    </div>
                    <a className="auth_btn" onClick={this.handleGroupJoin}>
                    </a>
                </form>
            </div>
        );
        const logInView = (
            <div>
                <header><img src="/img/viva_logo_big.png" alt="logo"/></header>
                <span className="auth_block__group-name">{this.props.groupName}</span>
                <span className="auth_block__group-title">Enter your ID and Select your Role</span>
                <form className="auth_block__form-login">
                    <div className="auth_block__input-filed">
                        <label htmlFor="icon_prefix">ID</label>
                        <input
                            id="input-userName"
                            name="userName"
                            type="text"
                            className="validate"
                            onChange={this.handleChange}
                            value={this.state.userName}
                            onKeyPress={this.handleKeyPress}
                            autoFocus
                        />
                    </div>
                    <div className="auth_block__invalid-message">
                    {this.state.invalid ? invalidView : ''}
                    {this.state.isDuplicatedId ? isDuplicatedIdView : ''}
                    </div>
                    <div className="auth_block__input-filed">
                        <label>Role</label>
                        <div className="auth_block__checkbox-group">
                             <span className="auth_block__checkbox auth_block__checkbox--disable">
                                <label>Novice</label>
                                 <input type="checkbox" id="novice" value="on"/>
                             </span>
                            <span className="auth_block__checkbox">
                                <label>Expert</label>
                                <input type="checkbox" id="expert" value="on"/>
                            </span>
                        </div>
                        <i className="auth_block__icon-notice"><img src="/img/icon_notice.png" alt=""/></i>
                        <i className="auth_block__role-tip"><img className="" src="/img/role_tip.png" alt=""/></i>
                    </div>
                    <a className="auth_btn" onClick={this.handleLogin}></a>
                </form>
            </div>
        );

        return (
            <div className="auth_block col s12">
                {/*{ this.props.mode ? groupJoinView : logInView }*/}
                { logInView }
            </div>
        );
    }
}

Authentication.propTypes = propTypes;
Authentication.defaultProps = defaultProps;

export default Authentication;