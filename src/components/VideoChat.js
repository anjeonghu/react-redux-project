/**
 * Created by front on 2017-05-12.
 */
import React from 'react';
import './css/videoChat.css';
import vivarSDK from '../api/vivarSDK';

const propTypes = {
};

const defaultProps = {
};

class VideoChat extends React.Component {

    render() {
        return (
            <div className="video_block">
                <div className="video_block__remote-video">
                    <video id="remote-video" autoPlay="autoPlay" width="844" height="422" className="video" />
                </div>
                <div className="video_block__local-video">
                    <video id="local-video" autoPlay="autoPlay"  width="180" height="135" className="video" />
                </div>
            </div>
        );
    }

}

VideoChat.propTypes = propTypes;
VideoChat.defaultProps = defaultProps;

export default VideoChat;