/**
 * Created by front on 2017-05-10.
 */
import React from 'react';

import './css/groupMembers.css';
import {vivarSDK} from '../api';
const propTypes = {
    data: React.PropTypes.object,
    onSendCall: React.PropTypes.func
};

const defaultProps = {
    data: {},
    onSendCall: () => {
        console.log("onSendCal function does not defined")
    }
};

class GroupMembers extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedIndex: -1,
            active: false
        };
        this.onSelected = this.onSelected.bind(this);
    }

    componentDidMount() {

    }


    onSelected(index, member) {
        if (member.state === 'busy' || (this.props.mode !== 'INIT' && this.props.mode !== 'CALL_DISCONNECTED')) {
            alert('you can call to member on busy');
        }
        else {
            this.setState({
                selectedIndex: index,
                active: true
            });
            this.props.onSendCall(member);
        }

    }

    render() {

        const filterArray = Object.keys(this.props.data).filter((Id, index) => {
            const member = this.props.data[Id];
            return ((member.state === 'alive' || member.state === 'busy') && vivarSDK.me.id !== Id)
        });

        const mapToComponents = (data) => {

            return data.map((Id, index) => {
                const member = this.props.data[Id];

                    let selectedClass;

                    selectedClass = (this.state.selectedIndex === index && this.state.active) ? 'novice-list__items--active' : '';

                    if (this.props.mode === 'CALL_DISCONNECTED' || this.props.mode === 'CALL_CONNECTED') {
                        selectedClass = '' // selected active 초기화
                    }
                    let memberState = (member.state === 'alive');

                    var availableClass = 'novice-list__items--available';
                    var busyClass = 'novice-list__items--busy';

                    return (
                        <li className={`novice-list__items ${selectedClass}`} key={index} data-member={member}
                            onClick={() => (this.onSelected(index, member))}>
                            <div >
                                <i className="novice-list__profile-icon ">
                                    <img src="/img/user_icon_list.png" alt=""/>
                                </i>
                                <span className="novice-list__items-name">{member.memberId.split('_')[1]}</span>
                                <span className="novice-list__items-status">
                                    <span
                                        className={memberState ? availableClass : busyClass}>{memberState ? 'Available' : 'Busy'}</span>
                                </span>
                            </div>
                        </li>
                    )

            });
        };
        return (
            <div className="novice-list col s3">
                <div className="novice-list__total-member">Novice List ({filterArray.length})</div>
                <ul>
                    {mapToComponents(filterArray)}
                </ul>
            </div>
        );
    }
}

GroupMembers.propTypes = propTypes;
GroupMembers.defaultProps = defaultProps;

export default GroupMembers;