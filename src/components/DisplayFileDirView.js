/**
 * Created by front on 2017-05-12.
 */
import React from 'react';
import './css/displayFileDirView.css';
import $ from 'jquery';
import {vivarSDK, getDirectionExpl} from '../api';

const propTypes = {
    mode: React.PropTypes.string,
    imageSrc: React.PropTypes.string,
    direction: React.PropTypes.string,
    file: React.PropTypes.any,
};

const defaultProps = {};

class DisplayFileView extends React.Component {

    constructor(props) {
        super(props);

        this.onCancel = this.onCancel.bind(this);
    }

    onCancel() {
        vivarSDK.sendCommand('ImageRemote', 'Off');

        $('#display-file-dir').css({opacity: 0, visibility: 'hidden'});
    }

    render() {

        let renderView = (<div></div>);

        let className ='';

        if (this.props.mode === 'DIRECTION') {
            let directionName = this.props.direction;
            let directionExpl = getDirectionExpl(directionName);

            const directionView = (

                <div className="display_file_dir_block__dir-wrapper">
                    <img id="display-direction" className="icon_expert_direction" src={`/img/icon_novice_direction_${directionName}.png`} alt=""/>
                    <div className="dir_explanation">
                        <span>{directionExpl}</span>
                    </div>
                </div>
            );
            renderView = directionView;
        }

        if (this.props.mode === 'FILE') {
            let file = this.props.file;

            if(file){
                className = 'display_file_dir_block__file';

                let regexp = /image\/.*/;

                const fileView = (

                    <div className="display_file_dir_block__file-wrapper">
                        <img id="display-file" className="icon_expert_file" src={this.props.imageSrc} alt="" width="641" height="384"/>
                    </div>
                );

                renderView =  regexp.test(file.type)? fileView:(<div></div>);
            }

        }

        return (
            <div id="display-file-dir" className={`display_file_dir_block ${className}`}>
                <main className="content">
                    {renderView}
                </main>
                <img id="" className="icon_expert_close" src='/img/icon_expert_direction_close.png' alt="" onClick={this.onCancel}/>
            </div>
        );
    }

}

DisplayFileView.propTypes = propTypes;
DisplayFileView.defaultProps = defaultProps;

export default DisplayFileView;