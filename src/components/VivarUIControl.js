/**
 * Created by front on 2017-05-16.
 */

import React from 'react';
import './css/vivarUIControl.css';
import $ from 'jquery';
import {vivarSDK, canvasUtill} from '../api';

const propTypes = {
    mode: React.PropTypes.string,
    onSendCapture: React.PropTypes.func,
    onSticker: React.PropTypes.func,
    onCancelDrawing: React.PropTypes.func,
    onSendDirection: React.PropTypes.func,
    onSendFile: React.PropTypes.func,
    onSelectColor: React.PropTypes.func,
    onCaptureStatus: React.PropTypes.string,
    capturedData: React.PropTypes.string,
    context: React.PropTypes.any
};

const defaultProps = {
    onSendCapture: () => {
        console.log('onSendCapture not defined')
    }
};

class VivarUIControl extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            drawing: {
                mode:''
            },
            mode: 'INIT'
        };

        this.onSendCapture = this.onSendCapture.bind(this);
        this.onSticker = this.onSticker.bind(this);
        this.onSendDrawing = this.onSendDrawing.bind(this);
        this.onCancelDrawing = this.onCancelDrawing.bind(this);
        this.onSendDirection = this.onSendDirection.bind(this);
        this.onCancelDirection = this.onCancelDirection.bind(this);
        this.onSendFile = this.onSendFile.bind(this);
        this.onSelectColor = this.onSelectColor.bind(this);
        this.onUndoDrawing = this.onUndoDrawing.bind(this);
        this.onChangeView = this.onChangeView.bind(this);
        this.onEndCall = this.onEndCall.bind(this);
        this.onDirectionFlicker = this.onDirectionFlicker.bind(this);
        this.onTooltipIn = this.onTooltipIn.bind(this);
        this.onTooltipOut = this.onTooltipOut.bind(this);
        this.onBackToUiInit = this.onBackToUiInit.bind(this);
    }

    componentDidMount() {

    }
    shouldComponentUpdate(nextProps, nextState) {
        if(this.props.isUiInit){
            this.setState({
               mode:'INIT'
            });
        }
        return true;
    }

    onChangeView(mode) {
        if (mode === 'DIRECTION') {
            // file, direction view , drawing 초기화
            vivarSDK.sendCommand('ImageRemote', 'Off'); // 파일 view 초기화
            $('#display-file-dir').css({opacity: 0, visibility: 'hidden'});
        }

        this.setState({
            drawing: {
                mode:''
            },
            mode: mode
        });
    }

    onSendCapture() {
        vivarSDK.sendCommand('ImageRemote', 'Off'); // 파일 view 초기화
        $('#display-file-dir').css({opacity: 0, visibility: 'hidden'}); //display file view 초기화

        this.props.onSendCapture();

        this.setState({
            mode: 'DRAWING'
        });
    }

    onCancelDrawing() {
        this.props.onCancelDrawing();
    }

    onSendDrawing() {
        this.props.onSendDrawing().then(
            (result) => {
                if (result) {
                    this.onChangeView('INIT');
                }
            }
        );
    }

    onSelectColor(color, mode) {

        let nextState = this.state.drawing[mode];
        this.props.onSelectColor(color);
        this.setState({
            drawing:{
                mode:mode
            }
        });
    }

    onSticker() {
        this.setState({
            drawing:{
                mode:'sticker'
            }
        });
        this.props.onSticker();
    }

    onUndoDrawing() {

        let canvasWidth = document.getElementById('remote-video').offsetWidth;
        let canvasHeight = document.getElementById('remote-video').offsetHeight;
        let ctx = this.props.context;

        ctx.clearRect(0, 0, canvasWidth, canvasHeight);
        $('.add-sticker').remove();

        const image = new Image();

        image.onload = function () {
            let pos = window.getComputedStyle(vivarSDK.config.remoteVideo).getPropertyValue('object-position').split(' '); // % 포지션만 지원
            canvasUtill.imageDrawingCover(ctx, image, 0, 0, canvasWidth, canvasHeight, pos);
        };
        image.src = "data:image/png;base64," + this.props.capturedData; //captured된 이미지를 display한다.

        this.props.onUndoDrawing();
    }

    onDirectionFlicker(direction) {
        $('#display-file-dir').css({opacity: 0, visibility: "hidden"});
        setTimeout(
            () => {
                this.onSendDirection(direction)
            }
            , 100);
    }

    onSendDirection(direction) {

        clearTimeout(this.timeout);

        this.props.onSendDirection(direction).then(
            (result) => {
                if (result) {
                    $('#display-file-dir').css({opacity: 1, visibility: 'visible'});

                    this.timeout = setTimeout(() => {
                        $('#display-file-dir').css({opacity: 0, visibility: "hidden"}).animate({opacity: 0}, 800);
                    }, 2500);

                }
            }
        );
    }

    onCancelDirection() {
        $('#display-file-dir').css({opacity: 0, visibility: 'hidden'});
        this.onChangeView('INIT');
    }

    onSendFile(e) {

        let file = e.target.files[0];
        let fileType = file.type;
        let regexp = /image\/.*/;

        this.props.onSendFile(file).then(
            (result) => {
                if (regexp.test(fileType)) {
                    $('#display-file-dir').css({opacity: 1, visibility: 'visible'});
                }
            }
        );

        this.setState({
            mode: 'FILE'
        });

        $('#upload').val(null);  // 초기화를 해줘야 onChange 이벤트가 제대로 작동한다.
    }

    onEndCall() {
        vivarSDK.endCall();
    }

    onBackToUiInit() {
        this.props.onCancelDrawing(); // drawing 초기화
        this.onChangeView('INIT');
    }

    onTooltipIn(e) {
        let offset = $(e.target).offset();
        let position = $(e.target).attr('data-position');
        let tooltip = $(e.target).attr('data-tooltip');
        let obj = {left: offset.left, top: offset.top};
        $('.custom_tooltip').text(tooltip);

        if (position === 'top') {
            obj['top'] = obj['top'] - 50;
        }
        else if (position === 'left') {
            obj['left'] = obj['left'] - ($('.custom_tooltip').width() + 20);
        }
        else if (position === 'right') {
            obj['left'] = obj['left'] + 40;
        }

        obj['display'] = 'block';
        $('.custom_tooltip').css(obj);
    }

    onTooltipOut() {
        let init = {left: 0, top: 0, margin: 0, display: 'none'};
        $('.custom_tooltip').css(init);
    }


    render() {

        var renderView = (<div></div>);

        const InitView = (
            <div className="video_block__control-btn">
                <main className="content">
                    <div className="video_block__control-btn-wrapper">
                        <span>
                            <img id="ardraw-start"
                                 src="/img/icon_expert_tools_direction.png"
                                 alt=""
                                 onClick={() => this.onChangeView('DIRECTION')}
                            />
                            <img id="draw-pen-red"
                                 src="/img/icon_share_file.png"
                                 alt=""
                                 onClick={() => $('#upload').trigger('click')}/>
                        </span>
                        <span>
                            <img id="draw-pen-white"
                                 src="/img/icon_ardraw_draw.png"
                                 alt=""
                                 onClick={this.onSendCapture}/>
                            <img id="draw-pen-black"
                                 src="/img/icon_ardraw_eraser.png"
                                 alt=""
                                 onClick={this.onCancelDrawing}/>
                            <input type="file" name="fileUpload" id="upload" style={{'display': 'none'}}
                                   onChange={this.onSendFile}
                            />
                        </span>
                    </div>
                    <span>
                         <a id="video-end-call" className="end-call"
                            onClick={this.onEndCall}
                         />
                    </span>
                </main>
            </div>

        );

        const drawModeView = (
            <div className="video_block__control-btn">
                <main className="content">
                    <div className="video_block__control-btn-wrapper">
                        <div>
                            <div className="draw-btn-wrapper">
                                <img className="draw-pen-red draw-btn"
                                     src="/img/icon_expert_draw_red.png"
                                     alt=""
                                />
                                <img className={`draw-pen-red draw-btn draw-btn-sel ${this.state.drawing.mode ==='red' ? 'active':''}`}
                                     src="/img/icon_expert_draw_sel.png"
                                     alt=""
                                     onClick={() => this.onSelectColor({'r': '255', 'g': '0', 'b': '0', 'a': '1'},'red')}
                                />
                            </div>
                            <div className="draw-btn-wrapper">
                                <img className="draw-pen-white draw-btn"
                                     src="/img/icon_expert_draw_white.png"
                                     alt=""
                                />
                                <img className={`draw-pen-white draw-btn draw-btn-sel ${this.state.drawing.mode === 'white' ? 'active':''}`}
                                     src="/img/icon_expert_draw_sel.png"
                                     alt=""
                                     onClick={() => this.onSelectColor({'r': '255', 'g': '255', 'b': '255', 'a': '1'},'white')}
                                />
                            </div>
                            <div className="draw-btn-wrapper">
                                <img className="draw-pen-black draw-btn"
                                     src="/img/icon_expert_draw_black.png"
                                     alt=""

                                />
                                <img className={`draw-pen-black draw-btn draw-btn-sel ${this.state.drawing.mode ==='black' ? 'active':''}`}
                                     src="/img/icon_expert_draw_sel.png"
                                     alt=""
                                     onClick={() => this.onSelectColor({'r': '0', 'g': '0', 'b': '0', 'a': '1'},'black')}
                                />
                            </div>
                            <div className="draw-btn-wrapper">
                                <img className="draw-sticker draw-btn"
                                     src="/img/icon_expert_draw_sticker_nor.png"
                                     alt=""
                                />
                                <img className={`draw-sticker draw-btn draw-btn-sel ${this.state.drawing.mode ==='sticker' && this.props.stickerMode === true ? 'active':''}`}
                                     src="/img/icon_expert_draw_sticker_sel.png"
                                     alt=""
                                     onClick={this.onSticker}
                                />
                            </div>
                            <div className="draw-btn-wrapper">
                                <img className="draw-undo draw-btn"
                                     src="/img/undo_icon.png"
                                     alt=""
                                />
                                <img className={`draw-undo draw-btn draw-btn-sel ${this.state.drawing.mode ==='undo' ? 'active':''}`}
                                     src="/img/undo_icon_sel.png"
                                     alt=""
                                     onClick={this.onUndoDrawing}
                                />
                            </div>

                            <img className="draw-send draw-btn"
                                 src="/img/draw_send_button_nor.png"
                                 alt=""
                                 onClick={this.onSendDrawing}
                            />
                            <img className="draw-cancel draw-btn"
                                 src="/img/draw_cancel_button_nor.png"
                                 alt=""
                                 onClick={this.onBackToUiInit}
                            />
                        </div>
                    </div>
                </main>
            </div>
        );
        const drawModeDisabledView = (
            <div className="video_block__control-btn">
                <main className="content">
                    <div className="video_block__control-btn-wrapper">
                        <div>
                            <img className="draw-pen-red draw-btn-disabled" src="/img/icon_expert_draw_red.png" alt=""/>
                            <img className="draw-pen-white draw-btn-disabled" src="/img/icon_expert_draw_white.png"
                                 alt=""/>
                            <img className="draw-pen-black draw-btn-disabled" src="/img/icon_expert_draw_black.png"
                                 alt=""/>
                            <img className="draw-sticker draw-btn-disabled" src="/img/icon_expert_draw_sticker_nor.png"
                                 alt=""/>
                            <img className="draw-undo draw-btn-disabled" src="/img/undo_icon.png" alt=""/>
                            <img className="draw-send draw-btn-disabled" src="/img/draw_send_button_nor.png" alt=""/>
                            <img className="draw-cancel draw-btn"
                                 src="/img/draw_cancel_button_nor.png"
                                 alt=""
                                 onClick={this.onBackToUiInit}
                            />
                        </div>
                    </div>
                </main>
            </div>
        );
        const directionView = (
            <div className="video_block__control-btn">
                <main className="content">
                    <div className="video_block__control-btn-wrapper">
                        <div className="video_block__control-direction ">
                            <div>

                                <img id="tooltip" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_a_forward.png"
                                     alt=""
                                     data-position="top"
                                     data-tooltip="Forward"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('PeopleMoveForward')}
                                />
                            </div>
                            <div>
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_a_center.png"
                                     alt=""/>
                            </div>
                            <div>
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_a_backward.png"
                                     alt=""
                                     data-position="left"
                                     data-tooltip="Backward"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('PeopleMoveBackward')}
                                />
                            </div>
                        </div>
                        <div className="video_block__control-direction ">
                            <div>
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_b_arr_left_top.png"
                                     alt=""
                                     data-position="top"
                                     data-tooltip="Top left"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('CameraUpperLeft')}
                                />
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_b_arr_top.png"
                                     alt=""
                                     data-position="top"
                                     data-tooltip="Up"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('CameraUp')}/>
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_b_arr_right_top.png"
                                     alt=""
                                     data-position="top"
                                     data-tooltip="Top right"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('CameraUpperRight')}
                                />
                            </div>
                            <div>
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_b_arr_left.png"
                                     alt=""
                                     data-position="left"
                                     data-tooltip="Left"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('CameraLeft')}
                                />
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_b_camera.png"
                                     alt=""
                                />
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_b_arr_right.png"
                                     alt=""
                                     data-position="right"
                                     data-tooltip="Right"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('CameraRight')}
                                />
                            </div>
                            <div>
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_b_arr_left_down.png"
                                     alt=""
                                     data-position="left"
                                     data-tooltip="Bottom left"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('CameraLowerLeft')}
                                />
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_b_arr_down.png"
                                     alt=""
                                     data-position="top"
                                     data-tooltip="Down"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('CameraDown')}
                                />
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_b_arr_right_down.png"
                                     alt=""
                                     data-position="right"
                                     data-tooltip="Bottom right"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('CameraLowerRight')}
                                />
                            </div>
                        </div>
                        <div className="video_block__control-direction ">
                            <div>
                                <img id="" className="icon_expert_direction" src="/img/icon_expert_direction_c_top.png"
                                     alt=""
                                     data-position="top"
                                     data-tooltip="Top view"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('ObjectTopView')}
                                />
                            </div>
                            <div>
                                <img id="" className="icon_expert_direction" src="/img/icon_expert_direction_c_left.png"
                                     alt=""
                                     data-position="left"
                                     data-tooltip="Left side"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('ObjectLeftSide')}
                                />
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_c_center.png"
                                     alt=""
                                />
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_c_right.png" alt=""
                                     data-position="right"
                                     data-tooltip="Right side"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('ObjectRightSide')}
                                />
                            </div>
                            <div>
                                <img id="" className="icon_expert_direction"
                                     src="/img/icon_expert_direction_c_bottom.png"
                                     alt=""
                                     data-position="top"
                                     data-tooltip="Bottom view"
                                     onMouseOver={this.onTooltipIn}
                                     onMouseOut={this.onTooltipOut}
                                     onClick={() => this.onDirectionFlicker('ObjectBottomView')}
                                />
                            </div>
                        </div>
                        <div className="video_block__control-direction ">
                            <img id="" className="icon_expert_direction" src="/img/icon_expert_direction_good.png"
                                 alt=""
                                 data-position="top"
                                 data-tooltip="Good"
                                 onMouseOver={this.onTooltipIn}
                                 onMouseOut={this.onTooltipOut}
                                 onClick={() => this.onDirectionFlicker('FeelGood')}
                            />
                        </div>
                        <img id="direction-close" className="icon_expert_direction"
                             src="/img/icon_expert_direction_close.png"
                             alt=""
                             onClick={() => this.onCancelDirection()}
                        />

                    </div>
                </main>
            </div>
        );
        if (this.props.drawingStatus === 'CAPTURE_WAITING'|| this.props.drawingStatus ==='SEND_DRAWING_WAITING') {
            renderView = drawModeDisabledView;
        }
        else {
            renderView = (this.state.mode === "INIT" || this.state.mode === "FILE") ? InitView : (this.state.mode === "DIRECTION") ? directionView : drawModeView;
        }

        return (
            <div>
                {this.props.isUiInit? InitView : renderView}
            </div>
        );
    }
}

VivarUIControl.propTypes = propTypes;
VivarUIControl.defaultProps = defaultProps;

export default VivarUIControl;