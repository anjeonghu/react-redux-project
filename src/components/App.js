/**
 * Created by front on 2017-04-18.
 */
import React, {Component, PropTypes} from 'react';
import Counter from './Counter';

class App extends Component {
    render() {
        return (
            <div>
                <Counter/>
            </div>
        );
    }
}

export default App;