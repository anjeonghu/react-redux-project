/**
 * Created by front on 2017-05-12.
 */
import React from 'react';
import './css/dataChat.css';
import vivarSDK from '../api/vivarSDK';
import $ from 'jquery';

const propTypes = {
    message: React.PropTypes.any,
    messageType: React.PropTypes.string,
    imageSrc: React.PropTypes.string
};

const defaultProps = {};

class ChatMessage extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidUpdate() {

    }

    render() {
        function formatAMPM() {
            let date = new Date();
            let hours = date.getHours();
            let minutes = date.getMinutes();
            let ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            let strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        }

        let messageView;

        if (this.props.messageType === 'file') {
            let file = this.props.message;
            let fileSize = file.size;
            fileSize = (fileSize > Math.pow(1024,2))? ((fileSize/1024)/1024).toFixed(2)+'MB':(fileSize/1024).toFixed(2)+'KB';

            messageView = (
                <div className='message'>
                <span className='icon-message'>
                <img className="icon-message-short" src="/img/messafe_icon_file.png" alt=""/>
                </span>
                    <span>{file.name}</span>
                    <span className='message-date'>{formatAMPM()}</span>
                    <div>{fileSize}</div><br/>
                </div>
            );
        }
        else if (this.props.messageType === 'image') {


            let file = this.props.message;
            let fileSize = file.size;
            fileSize = (fileSize > Math.pow(1024,2))? ((fileSize/1024)/1024).toFixed(2)+'MB':(fileSize/1024).toFixed(2)+'KB';

            messageView = (
                <div className='message'>
                <span className='icon-message'>
                <img className='icon-message-short' src='/img/message_icon_image_file.png' alt=''/>
                </span>
                    <span className="message_file_name">{file.name}</span>
                    <span className='message-date'>{formatAMPM()}</span>
                    <div>{fileSize}</div><br/>
                    <img id='' className='icon_message_thumbnail' src={this.props.imageSrc} alt='' width='200' height='120'/>
                </div>
            );
        }
        else if (this.props.messageType === 'link') {
            let regex = /((http(s?)):\/\/[\/\w.?=&-]*)/gi;
            let message = this.props.message;

            message = message.replace(regex,"<a target='_blank' href='$1'>$1</a>");

            messageView = (
                <div className='message'>
                <span className='icon-message'>
                <img className='icon-message-short' src='/img/message_icon_link.png' alt=''/>
                </span>
                    <span dangerouslySetInnerHTML={{ __html: message }}></span>
                    <span className='message-date'>{formatAMPM()}</span>
                </div>
            );
        }
        else if (this.props.messageType === 'text') {
            let message = this.props.message;

            messageView = (
                <div className='message'>
               <span className='icon-message'>
               <img className='icon-message-short' src='/img/message_icon_message.png' alt=''/>
               </span>
                    <span>{message}</span>
                    <span className='message-date'>{formatAMPM()}</span>
                </div>
            );
        }
        return (
            <div>
                {messageView}
            </div>
        );
    }

}

ChatMessage.propTypes = propTypes;
ChatMessage.defaultProps = defaultProps;

export default ChatMessage;