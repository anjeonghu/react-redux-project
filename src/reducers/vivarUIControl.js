/**
 * Created by front on 2017-05-10.
 */
/**
 * Created by lg on 2017-05-03.
 */
import * as types from '../actions/ActionTypes';

const initialState = {
    status:'INIT', //'INIT', 'DRAWING','DIRECTION'
    drawing:{
        status:'INIT',
        capturedData:{},
        polyLines:[]
    },
    direction:{
        status:'INIT',
        direction:''
    },
    file: {
        status:'INIT',
        data: undefined
    },
    error:''
};
export default function vivarUIControl(state = initialState, action) {

    switch(action.type) {
        case types.UI_STORE_RESET:
            console.log('action start: ',action);
            return {
                ...state,
                status:'INIT', //'INIT', 'DRAWING','DIRECTION'
                drawing:{
                    status:'INIT',
                    capturedData:{},
                    polyLines:[]
                },
                direction:{
                    status:'INIT',
                    direction:''
                },
                file: {
                    status:'INIT',
                    data: undefined
                },
                error:''
            };
        case types.SEND_DRAWING_CAPTURE:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'DRAWING',
                drawing:{
                    ...state.drawing,
                    status:action.status
                }
            };
        case types.SEND_DRAWING_CAPTURE_SUCCESS:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'DRAWING',
                drawing:{
                    ...state.drawing,
                    status:action.status,
                    capturedData: action.capturedData
                }
            };
        case types.SEND_DRAWING_CAPTURE_FAILURE:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'DRAWING',
                drawing:{
                    ...state.drawing,
                    status:action.status
                },
                error:action.errorMessage
            };
        case types.SEND_DRAWING:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'DRAWING',
                drawing:{
                    ...state.drawing,
                    status:action.status
                }
            };
        case types.SEND_DRAWING_SUCCESS:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'DRAWING',
                drawing:{
                    ...state.drawing,
                    status:action.status,
                    polyLines:action.polyLines
                }
            };
        case types.SEND_DRAWING_FAILURE:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'DRAWING',
                drawing:{
                    ...state.drawing,
                    status:action.status
                },
                error:action.errorMessage
            };
        case types.SEND_DRAWING_CLEAR_SUCCESS:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'INIT',
                drawing:{
                    ...state.drawing,
                    status:action.status
                }
            };
        case types.SEND_DRAWING_CLEAR_FAILURE:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'INIT',
                drawing:{
                    ...state.drawing
                },
                error:action.errorMessage
            };
        case types.SEND_DIRECTION:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'DIRECTION',
                direction:{
                    ...state.direction,
                    status:action.status
                }
            };
        case types.SEND_DIRECTION_SUCCESS:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'DIRECTION',
                direction:{
                    ...state.direction,
                    status:action.status,
                    direction:action.direction
                }
            };
        case types.SEND_DIRECTION_FAILURE:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'DIRECTION',
                direction:{
                    ...state.direction,
                    status:action.status
                },
                error:action.errorMessage
            };
        case types.SEND_FILE:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'FILE',
                file:{
                    ...state.file,
                    status:action.status
                }
            };
        case types.SEND_FILE_SUCCESS:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'FILE',
                file:{
                    ...state.file,
                    status:action.status,
                    data:action.file
                }
            };
        case types.SEND_FILE_FAILURE:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'FILE',
                file:{
                    ...state.file,
                    status:action.status
                },
                error:action.errorMessage
            };
        default:
            return state;
    }
}