/**
 * Created by lg on 2017-05-03.
 */
import * as types from '../actions/ActionTypes';

const initialState = {
    group:{
        status:'INIT'
    },
    login:{
        status:'INIT'
    },
    status: {
        isJoinedGroup: false,
        isLoggedIn: false,
        groupId:'',
        password:'',
        userName: '',
        groupName: ''
    },
    error:''
};
export default function authentication(state = initialState, action) {
    switch(action.type) {
        /* group join */
        case types.AUTH_GROUP_JOIN:
            console.log('action start: ',action);
            return {
                ...state,
                group:{
                  status: 'WAITING'
                }
            };
        case types.AUTH_GROUP_JOIN_SUCCESS:
            console.log('action start: ',action);
            return {
                ...state,
                group:{
                    status: 'SUCCESS'
                },
                status: {
                    ...state.status,
                    isJoinedGroup: true,
                    groupName: action.groupName,
                    password: action.password
                }
            };
        case types.AUTH_LOGIN:
            console.log('action start: ',action);
            return {
                ...state,
                login:{
                    status: 'WAITING'
                },
                status: {
                    ...state.status,
                    isLoggedIn: false,
                    userName: action.userName
                }
            };
        case types.AUTH_LOGIN_SUCCESS:
            console.log('action start: ',action);
            return {
                ...state,
                group:{
                    status: 'SUCCESS'
                },
                login:{
                    status: 'SUCCESS'
                },
                status: {
                    ...state.status,
                    isJoinedGroup: true,
                    isLoggedIn: true,
                    groupId:action.groupId,
                    userName: action.userName,
                    groupName: action.groupName
                }
            };
        case types.AUTH_LOGIN_FAILURE:
            console.log('action start: ',action);
            return {
                ...state,
                group:{
                    status: 'FAILURE'
                },
                status: {
                    ...state.status,
                    isLoggedIn: false
                }
            };
        case types.AUTH_LOGOUT:
            console.log('action start: ',action);
            return {
                ...state,
                group: {
                    status: 'INIT'
                },
                login: {
                    status: 'INIT'
                },
                status: {
                    isJoinedGroup: false,
                    isLoggedIn: false,
                    groupId: '',
                    userName: '',
                    groupName: ''
                }
            };
        case types.UPDATE_AUTH_STATUS:
            console.log('action start: ',action);
            return {
                ...state,
                status: action.sessionData
            };
        default:
            return state;
    }
}