/**
 * Created by front on 2017-05-10.
 */
/**
 * Created by lg on 2017-05-03.
 */
import * as types from '../actions/ActionTypes';

const initialState = {
    //'CALL_INIT','SEND_WAITING','ACCEP_WAITING',SEND_FAILURE','ACCEPT_FAILURE', 'CALL_CONNECTED', 'CALL_DISCONNECTED'
    status: 'INIT'
    , callInfo: {
        remoteId: '',
        remoteName: '',
        callTime: ''
    },
    error: ''
};
export default function sendAndAcceptCall(state = initialState, action) {

    switch (action.type) {
        case types.CALL_INIT:
            console.log('action start: ', action);
            return {
                ...state,
                status: 'CALL_INIT'
            };
        case types.CALL_CONNECTED:
            console.log('action start: ', action);
            return {
                ...state,
                status: 'CALL_CONNECTED'
            };
        case types.CALL_DISCONNECTED:
            console.log('action start: ', action);
            return {
                ...state,
                status: 'CALL_DISCONNECTED',
                callInfo: {
                    ...state.callInfo,
                    callTime:''
                }

            };
        case types.SEND_CALL:
            console.log('action start: ', action);
            return {
                ...state,
                status: 'SEND_WAITING',
                callInfo: {
                    ...state.callInfo,
                    remoteId: action.remoteId,
                    remoteName: action.remoteName
                }
            };
        case types.SEND_CALL_FAILURE:
            console.log('action start: ', action);
            return {
                ...state,
                status: 'SEND_FAILURE',
                error: action.errorMessage
            };
        case types.ACCEPT_CALL:
            console.log('action start: ', action);
            return {
                ...state,
                status: 'ACCEPT_WAITING',
                callInfo: {
                    ...state.callInfo,
                    remoteId: action.remoteId
                }
            };
        case types.ACCEPT_CALL_SUCCESS:
            console.log('action start: ', action);
            return {
                ...state,
                status: 'ACCEPT_SUCCESS',
                callInfo: {
                    ...state.callInfo,
                    remoteName: action.remoteName
                }
            };
        case types.ACCEPT_CALL_FAILURE:
            console.log('action start: ', action);
            return {
                ...state,
                status: 'ACCEPT_FAILURE',
                callInfo: {
                    ...state.callInfo,
                    remoteId: action.remoteId
                },
                error: action.errorMessage
            };
        case types.CALL_TIME_STAMP:
            return {
                ...state,
                callInfo: {
                    ...state.callInfo,
                    callTime: action.callTime
                },
                error: action.errorMessage
            };
        default:
            return state;
    }
}