/**
 * Created by front on 2017-04-25.
 */
import { combineReducers } from 'redux';
import authentication from './authentication';
import getGroupMembers from './getGroupMembers';
import sendAndAcceptCall from './sendAndAcceptCall';
import vivarUIControl from './vivarUIControl';
import dataChat from './dataChat';

const reducers = combineReducers({
    authentication, getGroupMembers, sendAndAcceptCall, vivarUIControl, dataChat
});

export default reducers;