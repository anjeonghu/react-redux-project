/**
 * Created by front on 2017-05-10.
 */
/**
 * Created by lg on 2017-05-03.
 */
import * as types from '../actions/ActionTypes';

const initialState = {
    status:'INIT',
    groupInfo: {
        memberList:{}
    },
    error:''
};
export default function getGroupMembers(state = initialState, action) {
    switch(action.type) {
        /* group join */
        case types.GET_GROUP_MEMBERS:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'WAITING'
            };
        case types.GET_GROUP_MEMBERS_SUCCESS:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'SUCCESS',
                groupInfo: {
                    memberList: action.memberList
                }
            };
        case types.GET_GROUP_MEMBERS_FAILURE:
            console.log('action start: ',action);
            return {
                ...state,
                status: 'FAILURE',
                groupInfo: {
                    memberList: action.memberList
                },
                error: action.errorMessage
            };
        case types.GET_GROUP_MEMBERS_UPDATE: //TODO update 로 store를 변경했을때  re-render 하지 않는다.
            console.log('action start: ',action);
            const memberList = state.groupInfo.memberList;
            memberList[action.member.id] = action.member;
            return {
                ...state,
                status: 'UPDATE',
                groupInfo: {
                    ...state.groupInfo,
                    memberList
                }
            };
        default:
            return state;
    }
}