/**
 * Created by front on 2017-05-10.
 */
/**
 * Created by lg on 2017-05-03.
 */
import * as types from '../actions/ActionTypes';

const initialState = {
    status:'INIT',
    messageList:[],
    error:''
};
export default function dataChat(state = initialState, action) {

    switch(action.type) {
        case types.DATA_CHAT_STORE_RESET:
            console.log('action start: ',action);
            return {
                ...state,
                status:'INIT',
                messageList:[],
                error:''
            };
        case types.SEND_MESSAGE:
            console.log('action start: ',action);
            return {
                ...state,
                status: action.status
            };
        case types.SEND_MESSAGE_SUCCESS:
            console.log('action start: ',action);
            return {
                ...state,
                status: action.status,
                messageList: [...state.messageList, {message: action.message, messageType: action.messageType, imageSrc:action.imageSrc}]
            };
        case types.SEND_MESSAGE_FAILURE:
            console.log('action start: ',action);
            return {
                ...state,
                status: action.status,
                error:action.errorMessage
            };
        default:
            return state;
    }
}